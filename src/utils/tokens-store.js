const accessTokenKey = 'current.user.access.token';
const refreshTokenKey = 'current.user.refresh.token';

export default {
    accessToken() {
        return localStorage.getItem(accessTokenKey);
    },

    refreshToken() {
        return localStorage.getItem(refreshTokenKey);
    },

    store({ accessToken, refreshToken }) {
        if (accessToken) {
            localStorage.setItem(accessTokenKey, accessToken);
        }

        if (refreshToken) {
            localStorage.setItem(refreshTokenKey, refreshToken);
        }
    },

    clear() {
        localStorage.removeItem(accessTokenKey);
        localStorage.removeItem(refreshTokenKey);
    }
};
