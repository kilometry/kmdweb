import { GeneralErrors } from "../stores/Login/LoginStore";

/**
 * Utility function that translates general error from the server by its code.
 * @param {*} generalError Object that contains the "code" property. The value of the code is used for the translation.
 */
export function translateGeneralError(generalError) {
    if (!generalError.code) {
        return generalError;
    }

    switch (generalError.code) {
        case GeneralErrors.INVALID_CREDENTIALS:
            return 'Неверный email или пароль.';
        case GeneralErrors.ACCOUNT_NOT_ACTIVE:
            return 'Ваша учётная запись не была активирована.';
        case GeneralErrors.DEFAULT_LOGIN_ERROR:
            return 'Во время входа произошла ошибка. ' +
                'Проверьте правильность введённых данных и попробуйте снова. ' +
                'Если ошибка повторится, свяжитесь с нами.';
        case GeneralErrors.DEFAULT_REGISTRATION_ERROR:
            return 'Во время регистрации произошла ошибка. ' +
                'Проверьте правильность введённых данных и попробуйте снова. ' +
                'Если ошибка повторится, свяжитесь с нами.';

        default:
            return 'Во время выполнения операции произошла ошибка. Пожалуйста, свяжитесь с нами.';
    }
}

/**
 * Utility function that translates general errors received as a response from the server.
 * @param {*} errors Object that contains "fields" and "general" properties. "general" contains the Array of codes.
 */
export function translateErrors(errors) {
    console.log(errors);

    if (!errors.general) {
        return errors;
    }

    return {
        ...errors,
        general: errors.general.map(generalError => translateGeneralError(generalError))
    };
}
