/**
 * Returns the right plural form for the noun.
 *
 * @export
 * @param {number} number
 * @param {string} one
 * @param {string} few
 * @param {number} many
 * @returns
 */
export function getPlural(number, one, few, many) {
    let n = Math.abs(number);
    n %= 100;

    if (n >= 5 && n <= 20) {
        return many;
    }

    n %= 10;

    if (n === 1) {
        return one;
    }

    if (n >= 2 && n <= 4) {
        return few;
    }

    return many;
}

/**
 * Returns either the first or the second version of the noun depending on the flag value.
 *
 * @export
 * @param {boolean} isFirst true if to return the first noun; false otherwise.
 * @param {string} first first noun.
 * @param {string} second second noun.
 * @returns
 */
export function getNoun(isFirst, first, second) {
    return isFirst ? first : second;
}
