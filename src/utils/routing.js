import { createLocation } from 'history';

const staticRoutes = {
    HOME: '/',
    ABOUT: '/about',
    PROMOTIONS: '/promotions',
    CHILDREN: '/children',
    FOR_PARTNERS: '/for-partners',
    BLOG: '/blog',
    POST: '/blog/:postId',
    EMAIL_CONFIRMED: '/email-confirmed',
    PAYMENT_RESULT: '/payment-result',
    RIDE_PAYMENT: '/ride-payment',
    RIDE_REQUEST: '/ride-request',
    PROFILE: '/profile',
    PROFILE_PAYMENTS: '/profile/payments',
    PROFILE_PROMO: '/profile/promotional-codes',
    BENEFACTOR_AGREEMENT: '/benefactor-agreement',
    ANY: '*'
};

const routingFunctions = {
    post(postId) {
        return staticRoutes.POST.replace(':postId', postId);
    }
};

/**
 * All routes available in the application
 */
export const Routes = {
    ...staticRoutes,
    ...routingFunctions
};

/**
 * Creates full URL which corresponds to the given route
 * @param route
 * @param history
 * @returns {string}
 */
export function createUrlForRoute(route, history) {
    const href = history.createHref(createLocation(route));
    return `${window.location.origin}${window.location.pathname}${href}`;
}
