/* eslint-disable import/prefer-default-export */

export function handleChange(component, event, name) {
    const targetName = arguments.length >= 3 ? name : event.target.name;

    component.setState({
        [targetName]: event.target.value
    });
}
