export default class ReactiveValue {
    constructor(initial = null) {
        this._value = initial;
        this._hasValue = arguments.length > 0;
        this._subscribers = [];
    }

    subscribe(callback) {
        this._subscribers.push(callback);

        if (this._hasValue) {
            callback(this._value);
        }

        return () => {
            const index = this._subscribers.indexOf(callback);
            if (index >= 0) {
                this._subscribers.splice(index, 1);
            }
        };
    }

    set(value) {
        this._hasValue = true;
        this._value = value;

        this._subscribers.forEach(callback => {
            callback(value);
        });
    }

    get() {
        return this._value;
    }
}
