/* eslint-disable no-console */

function inDevOnly(f) {
    return (...args) => {
        if (process.env.NODE_ENV === 'development') {
            f(...args);
        }
    };
}

export default {
    debug: inDevOnly(console.debug),
    log: inDevOnly(console.log),
    info: inDevOnly(console.info),
    dir: inDevOnly(console.dir),
    warn: console.warn,
    error: console.error
};
