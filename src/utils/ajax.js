import tokens from './tokens-store';

const contentTypeJson = { 'Content-Type': 'application/json' };

function accessTokenOrThrow() {
    const token = tokens.accessToken();
    if (!token) {
        throw new Error(
            'No access token configured. Are you trying to access authorized API endpoint without logging in first?'
        );
    }

    return token;
}

function getDefaultHeaders(authorize) {
    return authorize ? { Authorization: `Bearer ${accessTokenOrThrow()}` } : {};
}

function appendTo(data, appendable) {
    Object.keys(data).forEach(key => {
        appendable.append(key, data[key]);
    });

    return appendable;
}

const createHeaders = data => appendTo(data, new Headers());
const createForm = data => appendTo(data, new FormData());

async function tryConvertToJson(response) {
    const contentTypeHeader = response.headers.get('Content-Type');
    const isJson = contentTypeHeader && contentTypeHeader.indexOf('application/json') >= 0;
    const result = isJson ? await response.json() : { status: response.status, statusText: response.statusText };

    if (response.ok) {
        return result;
    }

    throw result;
}

export default {
    postJson(endpoint, data, authorize = true) {
        const headersData = {
            ...getDefaultHeaders(authorize),
            ...contentTypeJson
        };

        return fetch(endpoint, {
            method: 'POST',
            body: JSON.stringify(data),
            headers: createHeaders(headersData)
        }).then(tryConvertToJson);
    },

    postForm(endpoint, form, headers = {}, authorize = true) {
        const headersData = {
            ...getDefaultHeaders(authorize),
            ...headers
        };

        return fetch(endpoint, {
            method: 'POST',
            headers: createHeaders(headersData),
            body: createForm(form)
        }).then(tryConvertToJson);
    },

    getJson(endpoint, authorize = true) {
        return fetch(endpoint, {
            method: 'GET',
            headers: createHeaders(getDefaultHeaders(authorize))
        }).then(tryConvertToJson);
    }
};
