import React from 'react';
import { Container } from 'flux/utils';
import AppDispatcher from '../stores/AppDispatcher';

/**
 * Dispatches an action to the application dispatcher.
 *
 * @param {String} eventType - type of the event to dispatch;
 * @param {Object} [payload] - optional payload to attach to the dispatched action
 */
export function dispatch(eventType, payload = null) {
    AppDispatcher.dispatch({ type: eventType, payload });
}

/**
 * Wraps the React component class into the one attached to one or multiple flux stores. The name "connect" is
 * to show similarity to the similar "connect" function from Redux.
 *
 * @param {ReactClass|Function} ReactComponentClass - React component class or stateless component function
 * @param {[Array<FluxStore>]} stores - array of Flux stores to attach to the component
 * @param {Function} calculateProps - a function that returns props passed to an instance of ReactComponentClass
 * @returns {ConnectedComponent} - a React class which can be used to create components attached to specified stores
 */
export function connect(ReactComponentClass, stores, calculateProps) {
    class ConnectedComponent extends React.Component {
        static getStores() {
            return stores;
        }

        static calculateState(prevState, props) {
            return calculateProps(prevState, props);
        }

        render() {
            return <ReactComponentClass {...this.state} />;
        }
    }

    return Container.create(ConnectedComponent, { withProps: true });
}