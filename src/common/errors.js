export function extractErrorsFromResponse(errorResponse, defaultGeneralError) {
    const generalErrors = Array.isArray(errorResponse.generalErrors)
        ? errorResponse.generalErrors
        : [];

    const fieldErrors = Array.isArray(errorResponse.fieldErrors)
        ? errorResponse.fieldErrors
        : [];

    if (generalErrors.length === 0 && fieldErrors.length === 0 && defaultGeneralError) {
        generalErrors.push(defaultGeneralError);
    }

    const fields = fieldErrors.reduce((acc, prop) => {
        acc[prop.field.toLowerCase()] = prop.errors;
        return acc;
    }, {});

    return { fields, general: generalErrors };
}

export function getErrorMessages(errors, field) {
    const errorsForField = errors.fields !== undefined && errors.fields[field.toLowerCase()];
    return Array.isArray(errorsForField) ? errorsForField : [];
}
