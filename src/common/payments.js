import {PaymentMethod, PaymentStatus} from '../stores/Payment';

/**
 * Returns the right display name depending on the payment type.
 */
export function getPaymentMethodName(paymentMethod) {
    switch (paymentMethod) {
        case PaymentMethod.BePaid:
            return 'bePaid';

        case PaymentMethod.CashToCourier:
            return 'оплата курьеру';

        case PaymentMethod.CashInOffice:
            return 'оплата в офисе';

        case PaymentMethod.Ssis:
            return 'ЕРИП';

        default:
            throw new Error(`Unknown payment method "${paymentMethod}"`);
    }
}

/**
 * Returns the right icon title depending on the payment type.
 */
export function getPaymentStatusName(status) {
    switch (status) {
        case PaymentStatus.Successful:
            return 'Успешно';

        case PaymentStatus.Rejected:
            return 'Отказ';

        case PaymentStatus.Processing:
            return 'Обработка';

        case PaymentStatus.Cancelled:
            return 'Отмена';

        case PaymentStatus.Created:
            return 'Создан';

        default:
            throw new Error(`Unknown payment status "${status}"`);
    }
}