/* eslint-disable jsx-a11y/anchor-is-valid */

import React from 'react';
import { Button } from 'reactstrap';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { DesktopMenuView, MobileMenuView } from './user-menu-view';
import LoginStore, { UserTypes, LoginPopups, GeneralErrors } from '../stores/Login/LoginStore';
import LoginActions from '../stores/Login/LoginActions';
import { connect } from '../utils/flux-utils';
import { createUrlForRoute, Routes } from '../utils/routing';

// TODO this probably should be merged with DefaultUserType from LoginStore
const DefaultUserType = 'benefactor';

// TODO show message after confirmation email was sent
class UserMenuContainer extends React.Component {
    static propTypes = {
        view: PropTypes.oneOf([DesktopMenuView, MobileMenuView]).isRequired,
        history: PropTypes.object.isRequired,
        children: PropTypes.node,
        currentPopup: PropTypes.string,
        registrationUserType: PropTypes.string.isRequired,
        menuPopoverIsVisible: PropTypes.bool.isRequired,
        operationInProgress: PropTypes.bool.isRequired,
        errors: PropTypes.object.isRequired,
        lastLoginAttemptEmail: PropTypes.string,
        currentUser: PropTypes.object.isRequired
    };

    static defaultProps = {
        children: [],
        currentPopup: null,
        lastLoginAttemptEmail: null
    };

    _toggleMenuPopover() {
        if (this.props.menuPopoverIsVisible) {
            LoginActions.hideUserMenuPopover();
        } else {
            LoginActions.showUserMenuPopover();
        }
    }

    _toggleLoginPopup() {
        if (this.props.currentPopup === LoginPopups.LOGIN) {
            LoginActions.closePopup();
        } else {
            LoginActions.showLoginPopup();
        }
    }

    _toggleRegistrationSuccessPopup() {
        if (this.props.currentPopup === LoginPopups.REGISTRATION_SUCCESS) {
            LoginActions.closePopup();
        } else {
            LoginActions.showRegistrationSuccessPopup();
        }
    }

    _toggleRegistrationPopup(userType = DefaultUserType) {
        if (this.props.currentPopup === LoginPopups.REGISTRATION) {
            LoginActions.closePopup();
        } else if (userType === UserTypes.BENEFACTOR) {
            LoginActions.showRegisterSponsor();
        } else if (userType === UserTypes.FAMILY) {
            LoginActions.showRegisterFamily();
        }
    }

    async _sendConfirmationEmail(email) {
        const emailConfirmedRedirectUri = createUrlForRoute(Routes.EMAIL_CONFIRMED, this.props.history);
        const request = { email, emailConfirmedRedirectUri };
        LoginActions.sendConfirmationEmail(request);
    }

    _translateGeneralError(generalError, email = null) {
        if (!generalError.code) {
            return generalError;
        }

        switch (generalError.code) {
            case GeneralErrors.INVALID_CREDENTIALS:
                return 'Неверный email или пароль';
            case GeneralErrors.ACCOUNT_NOT_ACTIVE:
                return (
                    <div>
                        <div>Ваша учётная запись не была активирована</div>
                        <div>
                            <input
                                type="button"
                                className="btn btn-link"
                                value="Отправить повторно письмо об активации"
                                onClick={() => this._sendConfirmationEmail(email)}
                            />
                        </div>
                    </div>
                );
            case GeneralErrors.DEFAULT_LOGIN_ERROR:
                return (
                    'Во время входа произошла ошибка. ' +
                    'Проверьте правильность введённых данных и попробуйте снова. ' +
                    'Если ошибка повторится, свяжитесь с нами.'
                );
            case GeneralErrors.DEFAULT_REGISTRATION_ERROR:
                return (
                    'Во время регистрации произошла ошибка. ' +
                    'Проверьте правильность введённых данных и попробуйте снова. ' +
                    'Если ошибка повторится, свяжитесь с нами.'
                );

            default:
                return 'Во время выполнения операции произошла ошибка. Пожалуйста, свяжитесь с нами';
        }
    }

    _translateErrors(errors, email = null) {
        if (!errors.general) {
            return errors;
        }

        return {
            ...errors,
            general: errors.general.map(generalError => this._translateGeneralError(generalError, email))
        };
    }

    _onLogin({ email, password }) {
        LoginActions.login(email, password);
    }

    async _onLogout() {
        LoginActions.logout();
    }

    _onRegister(user) {
        const emailConfirmedRedirectUri = createUrlForRoute(Routes.EMAIL_CONFIRMED, this.props.history);
        const registrationRequest = {
            ...user,
            emailConfirmedRedirectUri
        };
        LoginActions.register(registrationRequest);
    }

    render() {
        const content = this.props.currentUser.authenticated ? (
            <div>
                {this.props.children}
                <Link to={Routes.PROFILE} onClick={() => this._toggleMenuPopover()}>
                    Личные данные
                </Link>
                <Link to={Routes.PROFILE_PAYMENTS} onClick={() => this._toggleMenuPopover()}>
                    Переводы
                </Link>
                <Link to={Routes.PROFILE_PROMO} onClick={() => this._toggleMenuPopover()}>
                    Промокоды
                </Link>
                <Button color="link" onClick={() => this._onLogout()}>
                    Выйти
                </Button>
            </div>
        ) : (
            <div>
                {this.props.children}
                <Button onClick={() => this._toggleLoginPopup()} color="link">
                    Вход
                </Button>
                <Button color="link" onClick={() => this._toggleRegistrationPopup()}>
                    Регистрация
                </Button>
            </div>
        );

        const View = this.props.view;

        return (
            <View
                toggleMenuPopover={() => this._toggleMenuPopover()}
                showMenuPopover={this.props.menuPopoverIsVisible}
                toggleLoginPopup={() => this._toggleLoginPopup()}
                showLoginPopup={this.props.currentPopup === LoginPopups.LOGIN}
                toggleRegistrationPopup={userType => this._toggleRegistrationPopup(userType)}
                showRegistrationPopup={this.props.currentPopup === LoginPopups.REGISTRATION}
                toggleRegistrationSuccessPopup={() => this._toggleRegistrationSuccessPopup()}
                showRegistrationSuccessPopup={this.props.currentPopup === LoginPopups.REGISTRATION_SUCCESS}
                userType={this.props.registrationUserType}
                onLogin={data => this._onLogin(data)}
                onRegister={data => this._onRegister(data)}
                registrationErrors={this._translateErrors(this.props.errors)}
                loginErrors={this._translateErrors(this.props.errors, this.props.lastLoginAttemptEmail)}
                userName={this.props.currentUser.firstName}
                isLoading={this.props.operationInProgress}
            >
                {content}
            </View>
        );
    }
}

export default connect(
    UserMenuContainer,
    [LoginStore],
    (prev, props) => ({
        ...LoginStore.getState(),
        ...props
    })
);
