import React from 'react';
import PropTypes from 'prop-types';
import { Modal, ModalBody, ModalHeader } from 'reactstrap';

const RegistrationSuccessPopup = ({ toggle, isOpen }) => (
    <Modal
        isOpen={isOpen}
        toggle={toggle}
        className="modal-custom"
        size="lg"
        autoFocus={false}
    >
        <ModalHeader toggle={toggle} />
        <ModalBody>
            <div className="modal-wrap text-center">
                <div className="modal-wrap-title">Регистрация завершена</div>
                <div className="modal-wrap-subtitle mt-3">
                    На указанный вами адрес электронной почты было отправлено письмо с инструкцией
                    о том, как активировать вашу учётную запись.
                </div>
            </div>
        </ModalBody>
    </Modal>
);

RegistrationSuccessPopup.propTypes = {
    toggle: PropTypes.func.isRequired,
    isOpen: PropTypes.bool.isRequired
};

export default RegistrationSuccessPopup;
