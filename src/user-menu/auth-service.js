import ReactiveValue from '../utils/reactive-value';
import asyncConfig from '../config';
import logger from '../utils/logger';
import ajax from '../utils/ajax';
import tokens from '../utils/tokens-store';

function basicAuth({ clientId, clientSecret }) {
    return {
        Authorization: `Basic ${btoa(`${clientId}:${clientSecret}`)}`
    };
}

const openIdConfigPromise = asyncConfig.then(({ openIdConfigurationEndpoint }) =>
    ajax.getJson(openIdConfigurationEndpoint, false));

export const currentUser = new ReactiveValue();

const anonymous = { firstName: '', lastName: '', authenticated: false };

let tokenRenewalHandle = null;

function isAuthenticated() {
    const user = currentUser.get();
    return user && user.authenticated;
}

function convertAccountToUserModel(account) {
    return {
        firstName: account.firstName || '',
        lastName: account.lastName || '',
        patronym: account.patronym || '',
        email: account.email || '',
        phoneNumber: account.phoneNumber || '',
        authenticated: true,
        avatarUrl: account.avatarUrl || '',
        userType: account.userType || ''
    };
}

async function retrieveUserAccount() {
    const config = await asyncConfig;
    const account = await ajax.getJson(`${config.apiEndpoint}/Account`);
    logger.dir(account);
    return account;
}

async function requestTokens(additionalFormData) {
    const config = await asyncConfig;
    const openIdConfig = await openIdConfigPromise;

    const formData = {
        scope: 'openid profile api1 offline_access roles',
        client_id: config.clientId,
        ...additionalFormData
    };

    const result = await ajax.postForm(
        openIdConfig.token_endpoint,
        formData,
        basicAuth(config),
        false
    );

    return {
        accessToken: result.access_token,
        refreshToken: result.refresh_token,
        expiresIn: result.expires_in
    };
}

async function refreshAccessToken() {
    logger.info('Refreshing access token');

    const authTokens = await requestTokens({
        grant_type: 'refresh_token',
        refresh_token: tokens.refreshToken()
    });

    logger.dir(authTokens);

    return authTokens;
}

function scheduleTokenRenewal(expiresIn) {
    if (tokenRenewalHandle) {
        clearTimeout(tokenRenewalHandle);
        tokenRenewalHandle = null;
    }

    tokenRenewalHandle = setTimeout(async () => {
        try {
            logger.info('Automatic refreshing access token...');
            const tokensResponse = await refreshAccessToken();

            if (isAuthenticated()) {
                tokens.store(tokensResponse);
                scheduleTokenRenewal(tokensResponse.expiresIn);
            }
        } catch (e) {
            logger.error('Refreshing token failed. No further attempts will be made.', e);
        }
    }, (expiresIn / 2) * 1000);
}

async function login(email, password) {
    const tokensResponse = await requestTokens({
        username: email,
        password,
        grant_type: 'password'
    });

    tokens.store(tokensResponse);

    const account = await retrieveUserAccount();
    logger.log('User account', account);

    const newUser = convertAccountToUserModel(account);

    logger.debug('Saved access token to the local storage');

    currentUser.set(newUser);
    logger.debug(`Logged in as ${newUser.name}`);

    scheduleTokenRenewal(tokensResponse.expiresIn);

    return newUser;
}

async function register(registrationRequest) {
    const config = await asyncConfig;
    return ajax.postJson(
        `${config.identityApiEndpoint}/identity/register`,
        registrationRequest,
        false
    );
}

async function initializeCurrentUser() {
    const accessToken = tokens.accessToken();
    const refreshToken = tokens.refreshToken();

    if (accessToken && refreshToken) {
        logger.debug('Access token found, refreshing token and retrieving current user details...');

        try {
            const tokensResponse = await refreshAccessToken(refreshToken);
            tokens.store(tokensResponse);
            const account = await retrieveUserAccount();
            const user = convertAccountToUserModel(account);
            currentUser.set(user);
            scheduleTokenRenewal(tokensResponse.expiresIn);
            return user;
        } catch (e) {
            logger.error('Failed to retrieve current user details. Continuing as not authenticated user', e);
            currentUser.set(anonymous);
            return anonymous;
        }
    } else {
        logger.debug('No access token found. Current user is not authenticated');

        currentUser.set(anonymous);
        return anonymous;
    }
}

async function logout() {
    const config = await asyncConfig;
    const openIdConfig = await openIdConfigPromise;

    if (tokenRenewalHandle) {
        clearTimeout(tokenRenewalHandle);
        tokenRenewalHandle = null;
    }

    currentUser.set(anonymous);

    tokens.clear();

    await ajax.postForm(openIdConfig.revocation_endpoint, {
        token: tokens.refreshToken(),
        token_type_hint: 'refresh_token'
    }, basicAuth(config));
}

async function sendConfirmationEmail(request) {
    const config = await asyncConfig;
    await ajax.postJson(
        `${config.identityApiEndpoint}/identity/sendConfirmationEmail`,
        request,
        false
    );
}

export const AuthService = {
    isAuthenticated,
    login,
    logout,
    register,
    initializeCurrentUser,
    sendConfirmationEmail
};
