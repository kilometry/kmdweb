import React from 'react';
import { Modal, ModalHeader, ModalBody, Form, FormGroup, Button, Input, Alert } from 'reactstrap';
import PropTypes from 'prop-types';
import { handleChange } from '../utils/react-utils';
import Loading from './../components/loading';
import { GeneralError } from '../components/form';

export default class LoginPopup extends React.Component {
    static propTypes = {
        isOpen: PropTypes.bool.isRequired,
        showRegistration: PropTypes.func.isRequired,
        toggle: PropTypes.func.isRequired,
        login: PropTypes.func.isRequired,
        errorMessages: PropTypes.object.isRequired,
        isLoading: PropTypes.bool.isRequired
    }

    constructor() {
        super();
        this.state = { email: '', password: '' };
        this._handleChange = name => evt => handleChange(this, evt, name);
    }

    _onLoginClick(evt) {
        if (this._validate()) {
            this.props.login({ email: this.state.email, password: this.state.password });
        }

        evt.preventDefault();
    }

    _validate() {
        const emailIsValid = Boolean(this.state.email);
        const passwordIsValid = Boolean(this.state.password);

        // setting to 'undefined' when valid because in this case
        // the input will not have any colored boarder -
        // by default if 'valid' is 'true', the input is highlighted
        // with green which is not what we want here
        this.setState({
            emailIsValid: emailIsValid ? undefined : false,
            passwordIsValid: passwordIsValid ? undefined : false
        });

        return emailIsValid && passwordIsValid;
    }

    _onKeyUp(evt) {
        if (evt.keyCode === 13) {
            this._onLoginClick(evt);
        }
    }

    _onPopupOpen() {
        this.setState({ email: '', password: '' });
    }

    _renderGeneralErrorMessage() {
        return this.props.errorMessages.general &&
            <Alert color="danger">{this.props.errorMessages.general}</Alert>;
    }

    render() {
        return (
            <Modal
                isOpen={this.props.isOpen}
                toggle={() => this.props.toggle()}
                className="modal-custom"
                size="lg"
                autoFocus={false}
                onOpened={() => this._onPopupOpen()}
            >
                <ModalHeader toggle={() => this.props.toggle()} />
                <ModalBody>
                    <Loading isVisible={this.props.isLoading} />

                    <div className="modal-wrap text-center">
                        <div className="modal-wrap-title">Вход</div>
                        <div className="modal-wrap-subtitle mt-3">Войдите, если вы уже зарегистрированы в нашем сервисе</div>
                        <Form onKeyUp={(evt) => this._onKeyUp(evt)}>
                            <GeneralError errors={this.props.errorMessages} />

                            <FormGroup>
                                <Input
                                    type="text"
                                    placeholder="Email"
                                    value={this.state.email}
                                    autoFocus
                                    onChange={this._handleChange('email')}
                                    valid={this.state.emailIsValid}
                                />
                            </FormGroup>

                            <FormGroup>
                                <Input
                                    type="password"
                                    placeholder="Пароль"
                                    value={this.state.password}
                                    onChange={this._handleChange('password')}
                                    valid={this.state.passwordIsValid}
                                />
                            </FormGroup>

                            <FormGroup>
                                <div className="forgot-password">
                                    <Button color="link" tabIndex={-1}>Забыли пароль?</Button>
                                </div>
                            </FormGroup>

                            <FormGroup>
                                <Button
                                    color="primary"
                                    className="btn-primary-custom"
                                    onClick={evt => this._onLoginClick(evt)}
                                    type="submit"
                                >
                                    Войти
                                </Button>
                            </FormGroup>

                        </Form>

                        <FormGroup>
                            <div className="modal-wrap-footertitle mt-4">Или зарегистрируйтесь</div>

                            <div className="mt-2">
                                <Button color="link" onClick={() => this.props.showRegistration('benefactor')}>
                                    как благотворитель
                                </Button>
                            </div>

                            <div className="mt-2">
                                <Button color="link" onClick={() => this.props.showRegistration('family')}>
                                    как семья с особенным ребёнком
                                </Button>
                            </div>
                        </FormGroup>
                    </div>
                </ModalBody>
            </Modal>
        );
    }
}
