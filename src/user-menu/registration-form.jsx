import React from 'react';
import PropTypes from 'prop-types';
import MaskedInput from 'react-maskedinput';
import { handleChange } from '../utils/react-utils';
import { getErrorMessages } from '../common/errors';
import { FormControl, GeneralError } from '../components/form';

export default class RegistrationForm extends React.Component {
    static propTypes = {
        type: PropTypes.oneOf(['benefactor', 'family']).isRequired,
        register: PropTypes.func.isRequired,
        errorMessages: PropTypes.object.isRequired
    }

    constructor(props) {
        super();
        this.state = {
            type: props.type,
            firstName: '',
            lastName: '',
            patronym: '',
            email: '',
            phoneNumber: '',
            password: '',
            passwordConfirmation: ''
        };

        this._handleChange = name => evt => handleChange(this, evt, name);
    }

    _onRegisterClick(evt) {
        this.props.register({
            firstName: this.state.firstName,
            lastName: this.state.lastName,
            patronym: this.state.patronym,
            email: this.state.email,
            phoneNumber: this.state.phoneNumber.replace(/[()_\-\s]/g, ''),
            password: this.state.password,
            passwordConfirmation: this.state.passwordConfirmation,
            userType: this.state.type
        });
        evt.preventDefault();
    }

    render() {
        return (
            <div>
                <div className="modal-wrap-title">Регистрация</div>

                <form>
                    <GeneralError errors={this.props.errorMessages} />

                    <div className="form-group mt-3">
                        <label>Хочу зарегистрироваться как*:</label>
                        <div className="form-check">
                            <input
                                type="radio"
                                name="type"
                                id="reg-benefactor"
                                className="form-check-input"
                                checked={this.state.type === 'benefactor'}
                                value="benefactor"
                                onChange={this._handleChange('type')}
                            />
                            <label className="label-custom mb-2 form-check-label" htmlFor="reg-benefactor">
                                Благотворитель
                            </label>
                        </div>

                        <div className="form-check">
                            <input
                                name="type"
                                type="radio"
                                id="reg-family"
                                className="form-check-input"
                                checked={this.state.type === 'family'}
                                value="family"
                                onChange={this._handleChange('type')}
                            />
                            <label className="label-custom mb-2 form-check-label" htmlFor="reg-family">
                                Семья с особенным ребёнком
                            </label>
                        </div>
                    </div>

                    <FormControl
                        label="Ваша фамилия*:"
                        type="text"
                        placeholder="Иванов"
                        value={this.state.lastName}
                        onChange={this._handleChange('lastName')}
                        errors={getErrorMessages(this.props.errorMessages, 'lastName')}
                        autoFocus
                    />

                    <FormControl
                        label="Ваше имя*:"
                        type="text"
                        placeholder="Иван"
                        value={this.state.firstName}
                        onChange={this._handleChange('firstName')}
                        errors={getErrorMessages(this.props.errorMessages, 'firstName')}
                    />

                    <FormControl
                        label="Ваше отчество*:"
                        type="text"
                        placeholder="Иванович"
                        value={this.state.patronym}
                        onChange={this._handleChange('patronym')}
                        errors={getErrorMessages(this.props.errorMessages, 'patronym')}
                    />

                    <FormControl
                        label="Email, по которому с вами можно связаться*:"
                        type="text"
                        placeholder="email@host.com"
                        value={this.state.email}
                        onChange={this._handleChange('email')}
                        errors={getErrorMessages(this.props.errorMessages, 'email')}
                    />

                    <FormControl
                        label="Номер телефона*:"
                        mask="+375 (11) 111-11-11"
                        value={this.state.phoneNumber}
                        onChange={this._handleChange('phoneNumber')}
                        renderControl={props => <MaskedInput {...props} />}
                        errors={getErrorMessages(this.props.errorMessages, 'phoneNumber')}
                    />

                    <FormControl
                        label="Пароль*:"
                        type="password"
                        placeholder="пароль"
                        value={this.state.password}
                        onChange={this._handleChange('password')}
                        errors={getErrorMessages(this.props.errorMessages, 'password')}
                    />

                    <FormControl
                        label="Повторите пароль*"
                        type="password"
                        placeholder="повторите пароль"
                        value={this.state.passwordConfirmation}
                        onChange={this._handleChange('passwordConfirmation')}
                        errors={getErrorMessages(this.props.errorMessages, 'passwordConfirmation')}
                    />

                    <div className="form-group text-center">
                        <button
                            type="submit"
                            className="btn btn-primary"
                            onClick={evt => this._onRegisterClick(evt)}
                        >
                            Зарегистрироваться
                        </button>
                    </div>
                </form>
            </div>
        );
    }
}
