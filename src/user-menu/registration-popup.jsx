import React from 'react';
import PropTypes from 'prop-types';
import { Modal, ModalHeader, ModalBody, Button } from 'reactstrap';
import RegistrationForm from './registration-form';
import Loading from './../components/loading';

const RegistrationPopup = props => (
    <Modal
        isOpen={props.isOpen}
        className="modal-custom registration-popup"
        toggle={() => props.toggle()}
        size="lg"
        autoFocus={false}
    >
        <ModalHeader toggle={() => props.toggle()} />
        <ModalBody>
            <Loading isVisible={props.isLoading} />

            <div className="modal-wrap text-center">
                <RegistrationForm
                    register={props.register}
                    type={props.type}
                    errorMessages={props.errorMessages}
                />
                <div className="login-suggestion">
                    <div>
                        Вы уже зарегистрировались?
                    </div>
                    <div>
                        <Button color="primary" onClick={props.showLogin}>Войти</Button>
                    </div>
                </div>
            </div>
        </ModalBody>
    </Modal>
);

RegistrationPopup.propTypes = {
    isOpen: PropTypes.bool.isRequired,
    type: PropTypes.string.isRequired,
    showLogin: PropTypes.func.isRequired,
    toggle: PropTypes.func.isRequired,
    register: PropTypes.func.isRequired,
    errorMessages: PropTypes.object.isRequired,
    isLoading: PropTypes.bool.isRequired
};

export default RegistrationPopup;
