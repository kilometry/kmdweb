/* eslint-disable no-return-assign,react/no-unused-prop-types */

import React from 'react';
import PropTypes from 'prop-types';
import $ from 'jquery';
import { Popover, PopoverBody } from 'reactstrap';
import LoginPopup from './login-popup';
import RegistrationPopup from './registration-popup';
import RegistrationSuccessPopup from './registration-success-popup';

class MenuView extends React.Component {
    static propTypes = {
        toggleMenuPopover: PropTypes.func.isRequired,
        showMenuPopover: PropTypes.bool.isRequired,
        toggleLoginPopup: PropTypes.func.isRequired,
        showLoginPopup: PropTypes.bool.isRequired,
        toggleRegistrationPopup: PropTypes.func.isRequired,
        showRegistrationPopup: PropTypes.bool.isRequired,
        toggleRegistrationSuccessPopup: PropTypes.func.isRequired,
        showRegistrationSuccessPopup: PropTypes.bool.isRequired,
        userType: PropTypes.string.isRequired,
        onLogin: PropTypes.func.isRequired,
        onRegister: PropTypes.func.isRequired,
        children: PropTypes.node.isRequired,
        registrationErrors: PropTypes.object.isRequired,
        loginErrors: PropTypes.object.isRequired,
        userName: PropTypes.string,
        isLoading: PropTypes.bool.isRequired
    }

    static defaultProps = {
        userName: ''
    }

    _renderLoginPopup() {
        return (
            <LoginPopup
                isOpen={this.props.showLoginPopup}
                showRegistration={this.props.toggleRegistrationPopup}
                toggle={this.props.toggleLoginPopup}
                login={this.props.onLogin}
                errorMessages={this.props.loginErrors}
                isLoading={this.props.isLoading}
            />
        );
    }

    _renderRegistrationPopup() {
        return (
            <RegistrationPopup
                isOpen={this.props.showRegistrationPopup}
                showLogin={this.props.toggleLoginPopup}
                toggle={this.props.toggleRegistrationPopup}
                register={this.props.onRegister}
                type={this.props.userType}
                errorMessages={this.props.registrationErrors}
                isLoading={this.props.isLoading}
            />
        );
    }

    _renderRegistrationSuccessPopup() {
        return (
            <RegistrationSuccessPopup
                isOpen={this.props.showRegistrationSuccessPopup}
                toggle={this.props.toggleRegistrationSuccessPopup}
            />
        );
    }
}

export class DesktopMenuView extends MenuView {
    render() {
        const openedClass = this.props.showMenuPopover ? 'menu-popover-open' : '';
        const userName = this.props.userName && <div className="user-name">{this.props.userName}</div>;

        return (
            <div className="col-12 col-lg">
                <div className={`d-flex flex-column align-items-center login-dropdown ${openedClass}`}>

                    <button type="button" className="btn btn-link" id="user-icon-button" onClick={this.props.toggleMenuPopover}>
                        <i className="fa fa-user-circle-o fa-lg" />
                        {userName}
                    </button>

                    <Popover
                        placement="bottom"
                        className="user-icon-menu desktop"
                        target="user-icon-button"
                        toggle={() => this.props.toggleMenuPopover()}
                        isOpen={this.props.showMenuPopover}
                    >
                        <PopoverBody>
                            <div className="dropdown-content">
                                <div className="icon"><i className="fa fa-caret-down fa-lg" /></div>
                                {this.props.children}
                            </div>
                        </PopoverBody>
                    </Popover>

                    {this._renderLoginPopup()}
                    {this._renderRegistrationPopup()}
                    {this._renderRegistrationSuccessPopup()}
                </div>
            </div>
        );
    }
}

export class MobileMenuView extends MenuView {
    componentDidUpdate(prevProps) {
        if (prevProps.showMenuPopover !== this.props.showMenuPopover) {
            $(this.popover).slideToggle(300);
        }
    }

    render() {
        return (
            <div>
                <button
                    type="button"
                    className={`btn btn-link mobile-menu-close ${this.props.showMenuPopover ? 'mobile-menu-open' : ''}`}
                    id="toggle-menu-button"
                    onClick={this.props.toggleMenuPopover}
                >
                    <span />
                    <span />
                    <span />
                </button>

                <div
                    className="user-icon-menu mobile"
                    ref={el => this.popover = el}
                >
                    <div className="dropdown-content ">
                        {this.props.children}
                    </div>
                </div>

                {this._renderLoginPopup()}
                {this._renderRegistrationPopup()}
                {this._renderRegistrationSuccessPopup()}
            </div>
        );
    }
}
