import React from 'react';
import { HashRouter, Route, Switch } from 'react-router-dom';
import Header from './components/header';
import PrivateRoute from './components/PrivateRoute';
import Profile from './components/Pages/Profile/ProfilePage/ProfilePage';
import Loading from './components/loading';
import About from './components/Pages/About/AboutPage/AboutPage';
import Partners from './components/Pages/Partners/PartnersPage/PartnersPage';
import Children from './components/Pages/Children/ChildrenPage/ChildrenPage';
import Blog from './components/Pages/Blog/BlogPage/BlogPage';
import Post from './components/Pages/Blog/PostPage/PostPage';
import Promotions from './components/Pages/Promotions/PromotionsPage/PromotionsPage';
import Home from './components/Pages/Home/HomePage/HomePage';
import PageFooter from './components/Pages/PageFooter/PageFooter';
import EmailConfirmed from './components/Pages/EmailConfirmed/EmailConfirmed';
import RidePayment from './components/Pages/RidePayment/RidePaymentPage/RidePaymentPage';
import RideRequest from './components/Pages/RideRequest/RideRequestPage/RideRequestPage';
import PaymentResult from './components/Pages/PaymentResult/PaymentResult';
import BenefactorAgreement from './components/Pages/BenefactorAgreement/BenefactorAgreement';
import { Routes } from './utils/routing';

import menuItems from './content/footer-links.json';

import './components/Pages/layout.css';
import LoginActions from './stores/Login/LoginActions';

const NotImplemented = () => (
    <div style={{ paddingTop: 150, textAlign: 'center' }}>
        <h2>Не сегодня</h2>
    </div>
);

export default class App extends React.Component {
    constructor() {
        super();
        this._updateIsMobile = this._updateIsMobile.bind(this);
        this.state = { isLoading: true };
    }

    async componentWillMount() {
        this._updateIsMobile();
        window.addEventListener('resize', this._updateIsMobile);

        await LoginActions.initializeCurrentUser();
        this.setState({
            isLoading: false
        });
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this._updateIsMobile);
    }

    _updateIsMobile() {
        this.setState({ mobile: window.innerWidth < 992 });
    }

    render() {
        return this.state.isLoading ? (
            <Loading isVisible />
        ) : (
            <HashRouter>
                <div className="wrapper">
                    <Header mobile={this.state.mobile} />
                    <div className="content">
                        <Switch>
                            <Route exact path={Routes.HOME} component={Home} />
                            <Route path={Routes.ABOUT} component={About} />
                            <Route path={Routes.FOR_PARTNERS} component={Partners} />
                            <Route path={Routes.CHILDREN} component={Children} />
                            <Route path={Routes.BLOG} exact component={Blog} />
                            <Route path={Routes.POST} component={Post} />
                            <Route path={Routes.PROMOTIONS} component={Promotions} />
                            <Route path={Routes.RIDE_PAYMENT} component={RidePayment} />
                            <Route path={Routes.RIDE_REQUEST} component={RideRequest} />
                            <PrivateRoute redirect={Routes.HOME} path={Routes.PROFILE} component={Profile} />
                            <Route path={Routes.EMAIL_CONFIRMED} component={EmailConfirmed} />
                            <Route path={Routes.PAYMENT_RESULT} component={PaymentResult} />
                            <Route path={Routes.BENEFACTOR_AGREEMENT} component={BenefactorAgreement} />
                            <Route path={Routes.ANY} component={NotImplemented} />
                        </Switch>
                    </div>
                    <PageFooter menuItems={menuItems.links} />
                </div>
            </HashRouter>
        );
    }
}
