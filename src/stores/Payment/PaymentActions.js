import { dispatch } from '../../utils/flux-utils';
import ajax from '../../utils/ajax';
import asyncConfig from '../../config';
import { isValidPaymentMethod } from './PaymentMethod';
import PaymentActionType from './PaymentActionType';
import { isValidPaymentStatus } from './PaymentStatus';
import { extractErrorsFromResponse } from '../../common/errors';

class PaymentActions {
    /**
     * saves new payment to server
     */
    async addNewPayment(payment) {
        dispatch(PaymentActionType.ADD_PAYMENT_START, payment);

        try {
            const result = await addNewPayment(payment);
            if (result.paymentUrl) {
                window.location.replace(result.paymentUrl);
            } else {
                dispatch(PaymentActionType.ADD_PAYMENT_SUCCESS, { payment: result });
            }
        } catch (errorResponse) {
            const errors = extractErrorsFromResponse(errorResponse, 'Произошла ошибка во время совершения платежа');
            dispatch(PaymentActionType.ADD_PAYMENT_ERROR, { errors });
        }
    }

    /**
     * Retrieves a list of user's payments.
     */
    async getPayments() {
        dispatch(PaymentActionType.GET_PAYMENTS_START);

        try {
            const payments = await loadPayments();
            dispatch(PaymentActionType.GET_PAYMENTS_SUCCESS, { payments });
        } catch (errorResponse) {
            const errors = extractErrorsFromResponse(
                errorResponse,
                'Произошла ошибка во время получения списка платежей'
            );

            dispatch(PaymentActionType.GET_PAYMENTS_ERROR, { errors });
        }
    }

    async loadPaymentMethods() {
        dispatch(PaymentActionType.LOAD_PAYMENT_METHODS_START);
        try {
            const config = await asyncConfig;
            const paymentMethods = await ajax.getJson(`${config.apiEndpoint}/payments/availableMethods`, false);
            dispatch(PaymentActionType.LOAD_PAYMENT_METHODS_SUCCESS, { paymentMethods });
        } catch (errorResponse) {
            const errors = extractErrorsFromResponse(
                errorResponse,
                'Произошла ошибка во время загрузки доступных способов оплаты'
            );

            dispatch(PaymentActionType.LOAD_PAYMENT_METHODS_ERROR, { errors });
        }
    }

    /**
     * Initializes the process of a new payment creation (on the front-end side only)
     */
    createNewPayment() {
        dispatch(PaymentActionType.CREATE_NEW_PAYMENT);
    }

    initializeCurrentPayment(paymentId) {
        dispatch(PaymentActionType.CURRENT_PAYMENT_INITIALIZE, { paymentId });
        return this.updateCurrentPayment(paymentId);
    }

    async updateCurrentPayment(paymentId) {
        try {
            const config = await asyncConfig;
            const payment = await ajax.getJson(`${config.apiEndpoint}/payments/${paymentId}`);
            dispatch(PaymentActionType.CURRENT_PAYMENT_UPDATE, { payment });
        } catch (error) {
            dispatch(PaymentActionType.CURRENT_PAYMENT_ERROR, { error });
        }
    }
}

export default new PaymentActions();

// Private functions

/**
 * Loads user's payments from server
 */
async function loadPayments() {
    const config = await asyncConfig;
    const payments = await ajax.getJson(`${config.apiEndpoint}/payments`, true);

    return payments.map(decodePayment).sort(sortByDateDesc);
}

/**
 * Saves new payment to the server
 */
async function addNewPayment(payment) {
    const config = await asyncConfig;
    return ajax.postJson(`${config.apiEndpoint}/payments/pay`, payment);
}

function sortByDateDesc(paymentA, paymentB) {
    return paymentB.date - paymentA.date;
}

/**
 * Validates the payment retrieved from the server.
 * Converts it to the client side object (parses dates and numbers).
 */
function decodePayment(payment) {
    const { id, amount, kilometers, method, paymentDate, status } = payment;

    if (!isValidPaymentMethod(method)) {
        throw new Error(`Error while getting the payment from the server. Unknown method type: ${method}`);
    }

    if (!isValidPaymentStatus(status)) {
        throw new Error(`Error while getting the payment from the server. Unknown status: ${status}`);
    }

    return {
        id,
        date: new Date(paymentDate),
        amount: parseFloat(amount),
        kilometers: parseFloat(kilometers),
        method,
        status
    };
}
