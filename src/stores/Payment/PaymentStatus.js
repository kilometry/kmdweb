import { values, includes } from 'lodash';

const paymentStatusesMap = {
    Created: 'created',
    Processing: 'processing',
    Successful: 'successful',
    Rejected: 'rejected',
    Cancelled: 'cancelled'
};

const paymentStatuses = values(paymentStatusesMap);

export default paymentStatusesMap;

export function isValidPaymentStatus(status) {
    return includes(paymentStatuses, status);
}
