import AppDispatcher from '../AppDispatcher';
import LoggingReduceStore from '../LoggingReduceStore';
import PaymentActionType from './PaymentActionType';
import EditingObjectState from '../EditingObjectState';
import LoginActionTypes from '../Login/LoginActionTypes';

class PaymentStore extends LoggingReduceStore {
    constructor() {
        super(AppDispatcher);
    }

    getInitialState() {
        return {
            payments: [],
            newPayment: {
                availablePaymentMethods: [],
                prerequisitesLoaded: false,
                state: EditingObjectState.Editing,
                errors: {}
            },
            newPaymentState: EditingObjectState.Editing,
            currentPayment: null,
            errors: {}
        };
    }

    [PaymentActionType.ADD_PAYMENT_START] = state => ({
        ...state,
        newPayment: {
            ...state.newPayment,
            state: EditingObjectState.Saving,
            errors: {}
        },
        errors: {},
        newPaymentState: EditingObjectState.Saving
    });

    [PaymentActionType.ADD_PAYMENT_SUCCESS] = state => ({
        ...state,
        newPayment: {
            ...state.newPayment,
            state: EditingObjectState.Success,
            errors: {}
        },
        error: {},
        newPaymentState: EditingObjectState.Success
    });

    [PaymentActionType.ADD_PAYMENT_ERROR] = (state, { errors }) => ({
        ...state,
        newPayment: {
            ...state.newPayment,
            state: EditingObjectState.Error,
            errors
        },
        errors,
        newPaymentState: EditingObjectState.Error
    });

    [PaymentActionType.CREATE_NEW_PAYMENT] = state => ({
        ...state,
        newPaymentState: EditingObjectState.Editing
    });

    [PaymentActionType.GET_PAYMENTS_START] = state => ({
        ...state,
        errors: {}
    });

    [PaymentActionType.GET_PAYMENTS_SUCCESS] = (state, { payments }) => ({
        ...state,
        error: {},
        payments
    });

    [PaymentActionType.CURRENT_PAYMENT_INITIALIZE] = (state, { paymentId }) => ({
        ...state,
        currentPayment: {
            id: paymentId,
            loaded: false
        }
    });

    [PaymentActionType.CURRENT_PAYMENT_UPDATE] = (state, { payment }) => {
        if (state.currentPayment.id.toLowerCase() === payment.id.toLocaleLowerCase()) {
            return {
                ...state,
                currentPayment: {
                    ...payment,
                    loaded: true
                }
            };
        }

        return state;
    };

    [PaymentActionType.LOAD_PAYMENT_METHODS_START] = state => ({
        ...state,
        newPayment: {
            ...state.newPayment,
            prerequisitesLoaded: false,
            errors: {}
        }
    });

    [PaymentActionType.LOAD_PAYMENT_METHODS_SUCCESS] = (state, { paymentMethods }) => ({
        ...state,
        newPayment: {
            ...state.newPayment,
            availablePaymentMethods: paymentMethods,
            prerequisitesLoaded: true,
            errors: {}
        }
    });

    [PaymentActionType.LOAD_PAYMENT_METHODS_ERROR] = (state, { errors }) => ({
        ...state,
        newPayment: {
            ...state.newPayment,
            errors
        }
    });

    [LoginActionTypes.LOGOUT_START] = state => ({
        ...state,
        payments: [],
        currentPayment: null,
        newPaymentState: EditingObjectState.Editing
    });
}

export default new PaymentStore();
