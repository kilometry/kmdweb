import { values, includes } from 'lodash';

const paymentMethodsMap = {
    CashToCourier: 'cashToCourier',
    BePaid: 'bePaid',
    CashInOffice: 'cashInOffice',
    Ssis: 'ssis'
};

const paymentMethods = values(paymentMethodsMap);

export default paymentMethodsMap;

export function isValidPaymentMethod(method) {
    return includes(paymentMethods, method);
}
