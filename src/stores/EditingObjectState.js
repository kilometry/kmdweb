/**
 * Provides set of statuses for new objects.
 * Can be used by stores to manage object creation lifecycle in state.
 * See PaymentStore for an example.
 */
const EditingObjectState = {
    Editing: "Editing",
    Saving: "Saving",
    Success: "Success",
    Error: "Error"
}

export default EditingObjectState;
