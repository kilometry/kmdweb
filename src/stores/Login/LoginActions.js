import LoginActionTypes from './LoginActionTypes';
import { dispatch } from '../../utils/flux-utils';
import { AuthService } from '../../user-menu';
import { extractErrorsFromResponse } from '../../common/errors';
import { GeneralErrors } from './LoginStore';

/**
 * Actions creator class for actions related with registration and logging in.
 */
export default {

    /**
     * Dispatches action to login user with the specified email and password.
     *
     * @param {String} email
     * @param {String} password
     */
    async login(email, password) {
        dispatch(LoginActionTypes.LOGIN_START, { email });
        try {
            const user = await AuthService.login(email, password);
            dispatch(LoginActionTypes.LOGIN_SUCCESS, { user });
        } catch (errorResponse) {
            const generalErrorCode = GeneralErrors.isKnown(errorResponse.error_description)
                ? errorResponse.error_description
                : GeneralErrors.DEFAULT_LOGIN_ERROR;

            const errors = extractErrorsFromResponse(
                errorResponse,
                { code: generalErrorCode }
            );

            dispatch(LoginActionTypes.LOGIN_ERROR, { errors });
        }
    },

    async register(registrationRequest) {
        dispatch(LoginActionTypes.REGISTRATION_START);
        try {
            await AuthService.register(registrationRequest);
            dispatch(LoginActionTypes.REGISTRATION_SUCCESS);
        } catch (errorResponse) {
            const errors = extractErrorsFromResponse(
                errorResponse,
                { code: GeneralErrors.DEFAULT_REGISTRATION_ERROR }
            );

            dispatch(LoginActionTypes.REGISTRATION_ERROR, { errors });
        }
    },

    /**
     * Sends email to the given email address with the link to activate user account
     *
     * @param {{email: String, emailConfirmedRedirectUri: String}} request
     * @returns {Promise<void>}
     */
    async sendConfirmationEmail(request) {
        dispatch(LoginActionTypes.SEND_CONFIRMATION_EMAIL_START);
        try {
            await AuthService.sendConfirmationEmail(request);
            dispatch(LoginActionTypes.SEND_CONFIRMATION_EMAIL_SUCCESS);
        } catch (errorResponse) {
            const errors = extractErrorsFromResponse(errorResponse);
            dispatch(LoginActionTypes.SEND_CONFIRMATION_EMAIL_ERROR, { errors });
        }
    },

    async logout() {
        dispatch(LoginActionTypes.LOGOUT_START);
        try {
            await AuthService.logout();
            dispatch(LoginActionTypes.LOGOUT_SUCCESS);
        } catch (errorResponse) {
            dispatch(LoginActionTypes.LOGOUT_ERROR, { errors: errorResponse });
        }
    },

    async initializeCurrentUser() {
        dispatch(LoginActionTypes.INITIALIZE_CURRENT_USER_START);
        const user = await AuthService.initializeCurrentUser();
        dispatch(LoginActionTypes.INITIALIZE_CURRENT_USER_COMPLETE, { user });
    },

    showRegisterFamily: () => dispatch(LoginActionTypes.SHOW_REGISTER_FAMILY),

    /**
     * Dispatches action to show register form for a sponsor.
     */
    showRegisterSponsor: () => dispatch(LoginActionTypes.SHOW_REGISTER_SPONSOR),

    showLoginPopup: () => dispatch(LoginActionTypes.SHOW_LOGIN_POPUP),

    /**
     * Closes any popup which might be opened (login, registration, reg. success)
     */
    closePopup: () => dispatch(LoginActionTypes.CLOSE_POPUP),

    showUserMenuPopover: () => dispatch(LoginActionTypes.SHOW_USER_MENU_POPOVER),

    hideUserMenuPopover: () => dispatch(LoginActionTypes.HIDE_USER_MENU_POPOVER),

    showRegistrationSuccessPopup: () => dispatch(LoginActionTypes.SHOW_REGISTRATION_SUCCESS_POPUP)
};
