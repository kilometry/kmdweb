import AppDispatcher from '../AppDispatcher';
import LoginActionTypes from './LoginActionTypes';
import LoggingReduceStore from '../LoggingReduceStore';

export const LoginPopups = {
    REGISTRATION: 'REGISTRATION',
    LOGIN: 'LOGIN',
    REGISTRATION_SUCCESS: 'REGISTRATION_SUCCESS'
};

export const UserTypes = {
    BENEFACTOR: 'benefactor',
    FAMILY: 'family'
};

const anonymous = { authenticated: false };

// Some of GeneralErrors constants are matched against error codes returned from the IdentityServer, so they
// can't be freely changed.

/**
 * Error codes which are known to the system and can be converted to user-friendly error messages on UI
 */
export const GeneralErrors = {
    INVALID_CREDENTIALS: 'invalid_username_or_password',
    ACCOUNT_NOT_ACTIVE: 'email_not_confirmed',

    DEFAULT_LOGIN_ERROR: 'login_failed',
    DEFAULT_REGISTRATION_ERROR: 'registration_failed',

    /**
     * Checks whether provided error code is known and can be converted to user-friendly error message
     * @param {String} errorCode
     * @returns {boolean}
     */
    isKnown(errorCode) {
        return Boolean(errorCode) && Object.values(this).indexOf(errorCode) >= 0;
    }
};

const defaultUserType = UserTypes.BENEFACTOR;

/**
 * The store that tracks the actions related to the registration and logging in.
 * Stores the state of the last dispatched action.
 *
 * @class LoginStore
 * @extends {LoggingReduceStore}
 */
class LoginStore extends LoggingReduceStore {
    constructor() {
        super(AppDispatcher);
    }

    /**
     * Returns true if the user is a benefactor, false otherwise.
     *
     * @memberof LoginStore
     */
    isBenefactor(user) {
        return user.userType === UserTypes.BENEFACTOR;
    }

    /**
     * Returns true if the user is a family representative, false otherwise.
     *
     * @memberof LoginStore
     */
    isFamily(user) {
        return user.userType === UserTypes.FAMILY;
    }

    getInitialState() {
        return {
            currentPopup: null,
            registrationUserType: defaultUserType,
            menuPopoverIsVisible: false,
            operationInProgress: false,
            errors: {},
            lastLoginAttemptEmail: null,
            showConfirmationEmailSentMessage: false,
            currentUser: anonymous
        };
    }

    [LoginActionTypes.SHOW_REGISTER_SPONSOR] = state => ({
        ...state,
        currentPopup: LoginPopups.REGISTRATION,
        registrationUserType: UserTypes.BENEFACTOR,
        menuPopoverIsVisible: false,
        errors: {}
    });

    [LoginActionTypes.SHOW_REGISTER_FAMILY] = state => ({
        ...state,
        currentPopup: LoginPopups.REGISTRATION,
        registrationUserType: UserTypes.FAMILY,
        menuPopoverIsVisible: false,
        errors: {}
    });

    [LoginActionTypes.SHOW_LOGIN_POPUP] = state => ({
        ...state,
        currentPopup: LoginPopups.LOGIN,
        registrationUserType: defaultUserType,
        menuPopoverIsVisible: false,
        showConfirmationEmailSentMessage: false,
        errors: {}
    });

    [LoginActionTypes.SHOW_REGISTRATION_SUCCESS_POPUP] = state => ({
        ...state,
        currentPopup: LoginPopups.REGISTRATION_SUCCESS,
        registrationUserType: defaultUserType,
        menuPopoverIsVisible: false,
        errors: {}
    });

    [LoginActionTypes.CLOSE_POPUP] = state => ({
        ...state,
        currentPopup: null,
        registrationUserType: defaultUserType,
        menuPopoverIsVisible: false,
        lastLoginAttemptEmail: null,
        errors: {}
    });

    [LoginActionTypes.SHOW_USER_MENU_POPOVER] = state => ({
        ...state,
        menuPopoverIsVisible: true
    });

    [LoginActionTypes.HIDE_USER_MENU_POPOVER] = state => ({
        ...state,
        menuPopoverIsVisible: false
    });

    [LoginActionTypes.LOGIN_START] = (state, { email }) => ({
        ...state,
        lastLoginAttemptEmail: email,
        operationInProgress: true
    });

    [LoginActionTypes.LOGIN_SUCCESS] = (state, { user }) => ({
        ...state,
        operationInProgress: false,
        currentPopup: null,
        lastLoginAttemptEmail: null,
        errors: {},
        currentUser: user
    });

    [LoginActionTypes.LOGIN_ERROR] = (state, { errors }) => ({
        ...state,
        operationInProgress: false,
        errors
    });

    [LoginActionTypes.REGISTRATION_START] = state => ({
        ...state,
        operationInProgress: true
    });

    [LoginActionTypes.REGISTRATION_SUCCESS] = state => ({
        ...state,
        operationInProgress: false,
        currentPopup: LoginPopups.REGISTRATION_SUCCESS,
        errors: {}
    });

    [LoginActionTypes.REGISTRATION_ERROR] = (state, { errors }) => ({
        ...state,
        operationInProgress: false,
        errors
    });

    [LoginActionTypes.SEND_CONFIRMATION_EMAIL_START] = state => ({
        ...state,
        operationInProgress: true
    });

    [LoginActionTypes.SEND_CONFIRMATION_EMAIL_SUCCESS] = state => ({
        ...state,
        operationInProgress: false,
        showConfirmationEmailSentMessage: true,
        errors: {}
    });

    [LoginActionTypes.SEND_CONFIRMATION_EMAIL_ERROR] = (state, { errors }) => ({
        ...state,
        operationInProgress: false,
        errors
    });

    [LoginActionTypes.LOGOUT_START] = state => ({
        ...state,
        menuPopoverIsVisible: false
    });

    [LoginActionTypes.LOGOUT_SUCCESS] = state => ({
        ...state,
        currentUser: anonymous
    });

    [LoginActionTypes.LOGOUT_ERROR] = state => ({
        ...state,
        currentUser: anonymous
    });

    [LoginActionTypes.INITIALIZE_CURRENT_USER_START] = state => ({
        ...state,
        operationInProgress: true,
        currentUser: null
    });

    [LoginActionTypes.INITIALIZE_CURRENT_USER_COMPLETE] = (state, { user }) => ({
        ...state,
        operationInProgress: false,
        currentUser: user
    });
}

export default new LoginStore();
