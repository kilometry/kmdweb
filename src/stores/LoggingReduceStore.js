/* eslint-disable no-console */

import { ReduceStore } from 'flux/utils';

/**
 * Adds logging functionality to {ReduceStore}. Logs dispatched actions, old and new state
 * only in non-production environments.
 * Because this store has "reduce" function implemented, when inheriting from this store override "reduceState"
 * instead of "reduce".
 * When dispatching an event with type: 'FOO_EVENT', and the class inherited from LoggingReduceStore has method
 * called "FOO_EVENT", then this method will be called instead of reduceState.
 * @abstract
 */
export default class LoggingReduceStore extends ReduceStore {
    constructor(dispatcher) {
        super(dispatcher);

        this._logActions =
            process.env.NODE_ENV !== 'production' ||
            (window.location.search && window.location.search.indexOf('logging') >= 0);
    }

    reduce(state, action) {
        if (!this._logActions) {
            return this._handleAction(state, action);
        }

        console.groupCollapsed(`${action.type} -> ${this.constructor.name}`);
        console.log('action: ', action);
        console.log('current state: ', state);

        try {
            const newState = this._handleAction(state, action);
            console.log('new state: ', newState);
            console.groupEnd();
            return newState;
        } catch (e) {
            console.error('reduce failed', e);
            console.groupEnd();
            throw e;
        }
    }

    _handleAction(state, action) {
        if (Object.prototype.hasOwnProperty.call(this, action.type) && typeof this[action.type] === 'function') {
            return this[action.type](state, action.payload);
        }

        return this.reduceState(state, action);
    }

    /**
     * Can be overridden in the derived class to handle dispatched action
     *
     * @param {Object} state - current state of the store
     * @param {Object} action - action dispatched to the store
     * @returns {Object} - new state
     */
    // eslint-disable-next-line no-unused-vars
    reduceState(state, action) {
        return state;
    }
}
