/* eslint-disable jsx-a11y/anchor-is-valid */

import React from 'react';
import { Link } from 'react-router-dom';
import { withRouter } from 'react-router';
import PropTypes from 'prop-types';
import logo from '../img/logo.png';
import { UserMenu, DesktopMenuView, MobileMenuView } from '../user-menu';
import LoginActions from '../stores/Login/LoginActions';
import { Routes } from '../utils/routing';

const RoutedUserMenu = withRouter(UserMenu);

function renderLogo() {
    return (
        <Link to={Routes.HOME} className="header-logo">
            <img src={logo} className="img-fluid" alt="Километры Добра" />
        </Link>
    );
}

function renderDesktopContent() {
    return (
        <div className="row position-relative align-items-center text-center">
            <div className="col-12 col-lg d-flex justify-content-between align-items-center">{renderLogo()}</div>
            <div className="col-12 col-lg-8 d-none d-lg-block">
                <ul className="main-menu">
                    <li>
                        <Link to={Routes.ABOUT}>О проекте</Link>
                    </li>
                    <li>
                        <Link to={Routes.PROMOTIONS}>Бонусы</Link>
                    </li>
                    <li>
                        <Link to={Routes.CHILDREN}>Дети</Link>
                    </li>
                    <li>
                        <Link to={Routes.FOR_PARTNERS}>Партнерам</Link>
                    </li>
                    <li>
                        <Link to={Routes.BLOG}>Новости</Link>
                    </li>
                </ul>
                <ul className="sub-menu mt-3">
                    <li>
                        {/* <Link to={Routes.RIDE_REQUEST} className="btn btn-link"> */}
                        {/* Заказать поездку */}
                        {/* </Link> */}
                    </li>
                    <li>
                        <Link to={Routes.RIDE_PAYMENT} className="btn btn-link">
                            Оплатить поездку
                        </Link>
                    </li>
                </ul>
            </div>

            <RoutedUserMenu view={DesktopMenuView} />
        </div>
    );
}

function onMobileLinkClick() {
    LoginActions.hideUserMenuPopover();
}

function renderMobileContent() {
    return (
        <div className="row position-relative align-items-center text-center">
            <div className="col-12 col-lg d-flex justify-content-between align-items-center">
                {renderLogo()}

                <RoutedUserMenu view={MobileMenuView}>
                    <Link to={Routes.ABOUT} onClick={onMobileLinkClick}>
                        О проекте
                    </Link>
                    <Link to={Routes.PROMOTIONS} onClick={onMobileLinkClick}>
                        Акции
                    </Link>
                    <Link to={Routes.CHILDREN} onClick={onMobileLinkClick}>
                        Дети
                    </Link>
                    <Link to={Routes.FOR_PARTNERS} onClick={onMobileLinkClick}>
                        Партнерам
                    </Link>
                    <Link to={Routes.BLOG} onClick={onMobileLinkClick}>
                        Блог
                    </Link>

                    {/* <Link to={Routes.RIDE_REQUEST} className="btn btn-link" onClick={onMobileLinkClick}> */}
                    {/* Заказать поездку */}
                    {/* </Link> */}

                    <Link to={Routes.RIDE_PAYMENT} className="btn btn-link" onClick={onMobileLinkClick}>
                        Оплатить поездку
                    </Link>
                </RoutedUserMenu>
            </div>
        </div>
    );
}

const Header = ({ mobile }) => (
    <div className="header">
        <div className="container">{mobile ? renderMobileContent() : renderDesktopContent()}</div>
    </div>
);

Header.propTypes = {
    mobile: PropTypes.bool.isRequired
};

export default Header;
