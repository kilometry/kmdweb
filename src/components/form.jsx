/* eslint-disable react/no-array-index-key */

import React from 'react';
import PropTypes from 'prop-types';

export const GeneralError = ({ errors }) => {
    let content;

    if (Array.isArray(errors.general)) {
        content = errors.general.map((msg, index) => <div key={index}>{msg}</div>);
    } else if (errors.general) {
        content = [errors.general];
    } else {
        content = [];
    }

    return content.length > 0
        ? <div className="alert alert-danger">{content}</div>
        : null;
};

GeneralError.propTypes = {
    errors: PropTypes.object.isRequired
};

const Errors = ({ errors }) => (
    <div className="invalid-feedback">
        {errors.map(msg => <div key={msg}>{msg}</div>)}
    </div>
);

Errors.propTypes = {
    errors: PropTypes.arrayOf(PropTypes.node)
};

Errors.defaultProps = {
    errors: []
};

export const FormControl = ({
    label, errors, renderControl, ...rest
}) => {
    const controlProps = {
        className: 'form-control',
        ...rest
    };

    if (errors.length > 0) {
        controlProps.className = `${controlProps.className || ''} is-invalid`;
    }

    const labelProps = controlProps.id
        ? { htmlFor: controlProps.id }
        : {};

    return (
        <div className="form-group">
            {label && <label {...labelProps}>{label}</label>}
            {renderControl(controlProps)}
            <Errors errors={errors} />
        </div>
    );
};

FormControl.propTypes = {
    label: PropTypes.string,
    renderControl: PropTypes.func,
    errors: PropTypes.arrayOf(PropTypes.string)
};

FormControl.defaultProps = {
    label: null,
    renderControl: props => <input {...props} />,
    errors: []
};
