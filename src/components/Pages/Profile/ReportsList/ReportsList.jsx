import React from 'react';
import w from './w.png';
import arrow from './arrow.svg';
import Pager from '../../Pager/Pager';
import {getPageItems} from '../../Pager/Pager';

import './reportsList.css';


export default class ReportsList extends React.Component {
    handleYearChange = (event) => {
        this.props.onYearChange(event.target.value);
    }

    handleReportClick = (report) => {
        return () => {
            this.props.onReportClick(report);
        };
    }

    render() {
        const { reports, activeYear, years } = this.props;
        const {activePage, itemsPerPage, onActivePageChange} = this.props;

        const items = getPageItems(reports, activePage, itemsPerPage).map((report) => {
            return (
                <div key={report.id} onClick={this.handleReportClick(report)} className="reports-list__card">
                    <div className="reports-list__card-image">
                        <img src={w} alt="скачать отчет" className="reports-list__word" />
                        <div className="reports-list__arrow-container">
                            <img src={arrow} alt="скачать отчет" />
                        </div>
                    </div>
                    {report.name}
                </div>
            );
        });

        return (
            <div className="tab-pane active">
                <div className="text-center">
                    <select id="years" onChange={this.handleYearChange} value={activeYear}>
                        {years.map((year) => {
                            return (<option key={year} value={year}>{year} год</option>);
                        })}
                    </select>
                </div>
                <div className="reports-list__cards-container">
                    {items}
                    <div className="reports-list__card"></div>
                    <div className="reports-list__card"></div>
                    <div className="reports-list__card"></div>
                    <div className="reports-list__card"></div>
                </div>
                <Pager
                    onActivePageChange={onActivePageChange}
                    itemsCount={reports.length}
                    activePage={activePage}
                    itemsPerPage={itemsPerPage}
                />
            </div>
        );
    }
}
