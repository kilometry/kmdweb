// Tabs component is just a container for data which is
// used by the parent Tabs component. So, no rendering
// for a particular tab is needed.
const Tab = () => false;

export default Tab;
