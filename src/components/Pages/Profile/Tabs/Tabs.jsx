import React from 'react';
import PropTypes from 'prop-types';
import { HashRouter, Route, NavLink, Switch, Redirect } from 'react-router-dom';

const Tabs = ({ match, children }) => {
    const childrenArray = React.Children.toArray(children);

    const header = childrenArray.map(child => (
        <NavLink key={child.props.route} activeClassName="active" to={child.props.route} className="nav-link">
            {child.props.title}
        </NavLink>
    ));

    const content = childrenArray.map(child => (
        <Route
            key={child.props.route}
            path={child.props.route}
            render={() => (
                <div className="tab-pane active">
                    {child.props.children}
                </div>)}
        />
    ));

    const defaultTab = childrenArray.find(child => child.props.isDefault);
    const redirect = defaultTab && (<Redirect to={defaultTab.props.route} />);

    return (
        <HashRouter basename={match.path}>
            <div className="col-12 col-md-9">
                <ul className="nav nav-pills mb-3 justify-content-center personal-data-navigation">
                    {header}
                </ul>
                <div className="tab-content">
                    <Switch>
                        {content}
                        {redirect}
                    </Switch>
                </div>
            </div>
        </HashRouter>
    );
};

Tabs.propTypes = {
    match: PropTypes.object.isRequired,
    children: PropTypes.node.isRequired
};

export default Tabs;
