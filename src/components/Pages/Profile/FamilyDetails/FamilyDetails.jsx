import React from 'react';
import ajax from '../../../../utils/ajax';
import moment from 'moment';

import RidesList from '../RidesList/RidesList';

import Tabs from '../Tabs/Tabs';
import Tab from '../Tabs/Tab';

export default class FamilyDetails extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            rides: [],
            activeYear: moment().year(),
            ridesActivePage: 0,
            itemsPerPage: 10
        };
    }

    componentWillMount() {
        // get the list of rides
        ajax.getJson("/profile/rides-list.json", true).then((list) => {
            this.setState({
                rides: list.rides,
            });
        });
    }

    handleYearChange = (year) => {
        this.setState({
            activeYear: parseInt(year, 10),
            ridesActivePage: 0
        });
    }

    handleRideCancel = (ride) => {
        /// TODO: request ride cancelation from the server
        console.log("cancel ride: " + ride.id);
    }

    handleRideCommentUpdate = (ride, comment) => {
        /// TODO: request ride comment update from the server
        console.log("update comment for ride: " + ride.id + " " + comment);
    }

    /**
     * Gets the list of years available for objects.
     * Each object should have the date field.
     *
     * @param {any} objects List of objects with the date field
     * @returns list of years available
     * @memberof Profile
     */
    getYearsList(objects) {
        const yearsList = [];

        objects.forEach((obj) => {
            const year = moment(obj.date).year();

            if (yearsList.indexOf(year) === -1) {
                yearsList.push(year);
            }
        });

        // sort years in descending order
        return yearsList.sort((a, b) => b - a);
    }

    /**
     * Updates the specified param in the state with the new page number.
     *
     */
    handleActivePageChange = (stateParam) => {
        return (page) => {
            this.setState({[stateParam]: page});
        }
    }

    render() {
        const { match } = this.props;
        const { rides, activeYear } = this.state;
        const {ridesActivePage, itemsPerPage} = this.state;

        const yearRides = rides.filter((ride) => moment(ride.date).year() === activeYear);

        return (
            <Tabs match={match}>
                <Tab title="Поездки" route="/rides" isDefault >
                    <RidesList
                        rides={yearRides}
                        years={this.getYearsList(rides)}
                        activeYear={activeYear}
                        onYearChange={this.handleYearChange}
                        onUpdateRideComment={this.handleRideCommentUpdate}
                        onCancelRide={this.handleRideCancel}
                        itemsPerPage={itemsPerPage}
                        activePage={ridesActivePage}
                        onActivePageChange={this.handleActivePageChange("ridesActivePage")}
                    />
                </Tab>
            </Tabs>
        );
    }
}
