import React from 'react';
import { Modal, ModalHeader, ModalBody, Label, Input } from 'reactstrap';
import Loading from '../../../loading';

import './changePassword.css';

export default class ChangePassword extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            password: "",
            newPassword: "",
            repeatedNewPassword: "",
            errors: [],
            isLoading: false
        };
    }

    /**
     * Updates values of input fields in the state.
     */
    handleChange = (event) => {
        const target = event.target;
        const value = target.type === "checkbox" ? target.checked : target.value;
        const name = target.id;

        this.setState({
            [name]: value
        });
    }

    handleSaveButtonClick = (event) => {
        event.preventDefault();

        if (this.validate()) {
            // TODO: add actual change password logic:
            // 1. validate new password on the server
            // 2. set new password
            console.log("Set new password: " + this.state.newPassword);
            this.props.toggle();
        } else {
            console.log("NOT VALID");
        }
    }

    validate() {
        const { password, newPassword, repeatedNewPassword } = this.state;
        const errors = [];

        if (password === "") {
            errors.push("Введите старый пароль.");
        }

        if (newPassword === "") {
            errors.push("Введите новый пароль.");
        }

        if (repeatedNewPassword === "") {
            errors.push("Повторите новый пароль.");
        }

        if (newPassword !== repeatedNewPassword) {
            errors.push("Новый пароль должен совпадать в полях \"Новый пароль\" и \"Повторите новый пароль\".");
        }

        this.setState({ errors: errors });

        if (errors.length === 0) {
            return true;
        } else {
            return false;
        }
    }

    render() {
        const { password, newPassword, repeatedNewPassword } = this.state;

        const { errors } = this.state;
        const errorsContent = errors.map((error, index) => (<div key={index}>{error}</div>));
        const errorsPanel = (errors.length !== 0) ? (<div className="alert alert-danger">{errorsContent}</div>) : (null);

        return (
            <Modal
                isOpen={this.props.isOpen}
                toggle={this.props.toggle}
                className="modal-custom"
                size="lg"
                autoFocus={false}
            >
                <ModalHeader toggle={this.props.toggle} />
                <ModalBody>
                    <Loading isVisible={this.state.isLoading} />

                    <div className="modal-wrap text-center">
                        <div className="modal-wrap-title">Изменить пароль</div>
                    </div>

                    {errorsPanel}

                    <form>
                        <div className="form-group">
                            <Label for="password">Старый пароль:</Label>
                            <Input
                                type="password"
                                name="password"
                                id="password"
                                value={password}
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="form-group">
                            <Label for="newPassword">Новый пароль:</Label>
                            <Input
                                type="password"
                                name="newPassword"
                                id="newPassword"
                                value={newPassword}
                                onChange={this.handleChange}
                            />
                        </div>
                        <div className="form-group">
                            <Label for="repeatedNewPassword">Повторите новый пароль:</Label>
                            <Input
                                type="password"
                                name="repeatedNewPassword"
                                id="repeatedNewPassword"
                                value={repeatedNewPassword}
                                onChange={this.handleChange}
                            />
                        </div>

                        <div className="form-group text-center">
                            <button id="submit" type="submit" className="btn btn-primary" onClick={this.handleSaveButtonClick}>
                                Изменить пароль
                            </button>
                        </div>

                    </form>
                </ModalBody>
            </Modal>
        );
    }
}
