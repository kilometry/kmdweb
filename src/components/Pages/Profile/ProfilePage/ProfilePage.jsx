import React from 'react';

import PersonalInfo from '../../../../profile/personal-info';
import BenefactorDetails from '../BenefactorDetails/BenefactorDetails';
import FamilyDetails from '../FamilyDetails/FamilyDetails';

import LoginStore from '../../../../stores/Login/LoginStore';

import './profilePage.css';
import { connect } from '../../../../utils/flux-utils';

class ProfilePage extends React.Component {
    render() {
        const { match, currentUser } = this.props;
        let details = (<div></div>);

        if (LoginStore.isBenefactor(currentUser)) {
            details = <BenefactorDetails match={match} />;
        } else if (LoginStore.isFamily(currentUser)) {
            details = <FamilyDetails match={match} />;
        }

        return (
            <div className="profile">
                <div className="title-container">Личный кабинет</div>
                <div className="container py-5">
                    <div className="row">
                        <PersonalInfo />
                        {details}
                    </div>
                </div>
            </div>
        );
    }
}

export default connect(ProfilePage, [LoginStore], (prev, props) => ({
    ...LoginStore.getState(),
    ...props
}));
