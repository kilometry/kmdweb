import React from 'react';
import classnames from 'classnames';
import moment from 'moment';
import Pager from '../../Pager/Pager';
import {getPageItems} from '../../Pager/Pager';

import './ridesList.css';

const RideStatus = {
    Taken: "taken",
    Canceled: "canceled",
    InProgress: "inProgress"
};

/**
 * Displays a list of family's rides
 *
 * @export
 * @class RidesList
 * @extends {React.Component}
 */
export default class RidesList extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            expandedRides: [],
            comments: []
        };
    }

    /**
     * fires the event to notify about selected year change.
     */
    handleYearChange = (event) => {
        this.props.onYearChange(event.target.value);
    }

    /**
     * Returns the right class name for the status depending on the ride status.
     */
    getStatusClass(ride) {
        if (ride.status === RideStatus.Taken) {
            return "rides-list__ride--taken";
        } else if (ride.status === RideStatus.Canceled) {
            return "rides-list__ride--canceled";
        } else if (ride.status === RideStatus.InProgress) {
            return "rides-list__ride--in-progress";
        }
    }

    /**
     * Returns the right class name for the row depending on the ride status.
     */
    getRowClass(ride) {
        if (this.isExpanded(ride)) {
            if (ride.status === RideStatus.Taken) {
                return "rides-list__ride-row--taken";
            } else if (ride.status === RideStatus.Canceled) {
                return "rides-list__ride-row--canceled";
            } else if (ride.status === RideStatus.InProgress) {
                return "rides-list__ride-row--in-progress";
            }
        } else {
            return "";
        }
    }

    /**
     * Returns the right name of the status depending on the ride status.
     */
    getStatus(ride) {
        if (ride.status === RideStatus.Taken) {
            return "состоялась";
        } else if (ride.status === RideStatus.Canceled) {
            return "отменена";
        } else if (ride.status === RideStatus.InProgress) {
            return "в обработке";
        }
    }

    /**
     * Manages which rides details are expanded by updating the component's state
     */
    toggleRideExpanded = (event) => {
        if (event !== undefined) {
            const rideId = event.target.id;
            const {expandedRides} = this.state;

            const index = expandedRides.indexOf(rideId);

            if (index === -1) {
                expandedRides.push(rideId);
            } else {
                expandedRides.splice(index, 1);
            }

            this.setState({expandedRides: expandedRides});
        }
    }

    /**
     * Fires the update comment event
     */
    handleCommentSubmit = (ride) => {
        return () => {
            this.props.onUpdateRideComment(ride, this.state.comments[ride.id]);
        }
    }

    /**
     * Changes the updated comment in the component's state
     */
    handleCommentChange = (ride) => {
        return (event) => {
            const {comments} = this.state;
            comments[ride.id] = event.target.value;

            this.setState({comments: comments});
        }
    }

    /**
     * Fires the ride cancelation event
     */
    handleRideCancel = (ride) => {
        return () => {
            this.props.onCancelRide(ride);
        }
    }

    /**
     * Checks whether the ride section is expanded or not.
     */
    isExpanded = (ride) => {
        return this.state.expandedRides.indexOf("moreInfo_" + ride.id) !== -1;
    }

    /**
     * Returns the right half of the ride details depending on the ride status:
     * - comment editing or dispaying for taken rides;
     * - cancelation button for in progress rides;
     * - empty for canceled rides;
     */
    getRideActionsSection = (ride) => {
        if (ride.status === RideStatus.Taken) {
            if (ride.comment !== "") {
                return (
                    <div>
                        <div>Нам очень важен ваш отзыв: </div>
                        <div>{ride.comment}</div>
                    </div>
                );
            } else {
                return (
                    <div>
                        <div>Нам очень важен ваш отзыв: </div>
                        <textarea value={this.state.comments[ride.id]} onChange={this.handleCommentChange(ride)} className="rides-list__ride-comment" />
                        <button onClick={this.handleCommentSubmit(ride)} className="btn btn-primary">Отправить</button>
                    </div>
                );
            }
        } else if (ride.status === RideStatus.Canceled) {
            return (<div></div>);
        } else if (ride.status === RideStatus.InProgress) {
            return (
                <div>
                    <div>Вы также можете отменить поездку:</div>
                    <button onClick={this.handleRideCancel(ride)} className="btn btn-primary">Отменить поездку</button>
                </div>
            );
        }
    }

    render() {
        const { activeYear, years, rides } = this.props;
        const {activePage, itemsPerPage, onActivePageChange} = this.props;

        const ridesPage = getPageItems(rides, activePage, itemsPerPage);

        const items = [];

        for (let i = 0; i < ridesPage.length; i++) {
            const ride = ridesPage[i];
            items.push((
                <tr key={ride.id} className={this.getRowClass(ride)}>
                    <td className="payment-code-cell">{ride.code}</td>
                    <td className="payment-date-cell">{moment(ride.date).format("DD/MM/YYYY")}</td>
                    <td className={classnames("payment-method-cell", this.getStatusClass(ride))}>{this.getStatus(ride)}</td>
                    <td className="">
                        <div id={"moreInfo_" + ride.id} className="rides-list__more-info" onClick={this.toggleRideExpanded}>
                            {(this.isExpanded(ride))?("закрыть"):("подробнее")}
                        </div>
                    </td>
                </tr>
            ));

            // if ride section is expanded, render the ride details section
            if (this.isExpanded(ride)) {
                items.push((
                    <tr key={"expanded_" + ride.id} className={this.getRowClass(ride)}>
                        <td colSpan="4" className="rides-list__more-info-cell">
                            <div className="rides-list__more-info-container">
                                <div className="rides-list__more-info-content">
                                    <div>
                                        <b>Откуда: </b>{ride.from}
                                    </div>
                                    <div>
                                        <b>Куда: </b>{ride.to}
                                    </div>
                                    <div>
                                        <b>Время: </b>{moment(ride.date).format("H:mm")}
                                    </div>
                                    <div>
                                        <b>Цель поездки: </b>{ride.description}
                                    </div>
                                    <div>
                                        {(ride.status === RideStatus.Taken)?("Поездка составила: " + ride.distance + " км"):("")}
                                    </div>
                                </div>
                                <div className="rides-list__more-info-actions">
                                    {this.getRideActionsSection(ride)}
                                </div>
                            </div>
                        </td>
                    </tr>
                ));
            }
        }

        const yearSelector = (
            <div className="text-center">
                <select id="years" onChange={this.handleYearChange} value={activeYear}>
                    {years.map((year) => {
                        return (<option key={year} value={year}>{year} год</option>);
                    })}
                </select>
            </div>
        );

        const tableHeader = (
            <thead>
                <tr>
                    <th>Номер поездки</th>
                    <th>Дата поездки</th>
                    <th>Статус</th>
                    <th></th>
                </tr>
            </thead>
        );

        return (
            <div className="tab-pane active">
                {yearSelector}
                <div>
                    <table className="table table-hover table-responsive-sm payments-table">
                        {tableHeader}
                        <tbody>
                            {items}
                        </tbody>
                    </table>
                </div>
                <Pager
                    onActivePageChange={onActivePageChange}
                    itemsCount={rides.length}
                    activePage={activePage}
                    itemsPerPage={itemsPerPage}
                />
            </div>
        );
    }
}
