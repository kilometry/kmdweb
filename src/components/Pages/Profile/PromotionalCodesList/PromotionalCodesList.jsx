import React from 'react';
import { Popover, PopoverBody } from 'reactstrap';
import classnames from 'classnames';
import moment from 'moment';
import Pager from '../../Pager/Pager';
import {getPageItems} from '../../Pager/Pager';

import './promotionalCodesList.css';

/**
 * Displays a list of promotional codes
 *
 * @export
 * @class PaymentsList
 * @extends {React.Component}
 */
export default class PromotionalCodesList extends React.Component {
    constructor(props) {
        super(props);

        this.state = { activePopoverId: "" };
    }

    /**
     * fires the event to notify about selected year change.
     */
    handleYearChange = (event) => {
        this.props.onYearChange(event.target.value);
    }

    getStatusClass(promotionalCode) {
        if (promotionalCode.status === "available") {
            return "promotional-codes-list__promotion--available";
        } else if (promotionalCode.status === "used") {
            return "promotional-codes-list__promotion--used";
        } else if (promotionalCode.status === "overdue") {
            return "promotional-codes-list__promotion--overdue";
        }
    }

    getStatus(promotionalCode) {
        if (promotionalCode.status === "available") {
            return "доступен";
        } else if (promotionalCode.status === "used") {
            return "потрачен";
        } else if (promotionalCode.status === "overdue") {
            return "просрочен";
        }
    }

    togglePromotionalCodePopover = (event) => {
        if (event === undefined || this.state.activePopoverId !== "") {
            this.setState({
                activePopoverId: ""
            });
        } else {
            this.setState({
                activePopoverId: event.target.id
            });
        }
    }

    render() {
        const { activeYear, years, promotionalCodes } = this.props;
        const {activePage, itemsPerPage, onActivePageChange} = this.props;

        const items = getPageItems(promotionalCodes, activePage, itemsPerPage).map((promotionalCode) => {
            return (
                <tr key={promotionalCode.id}>
                    <td className="payment-code-cell">{promotionalCode.code}</td>
                    <td className="payment-date-cell">{moment(promotionalCode.date).format("DD/MM/YYYY")}</td>
                    <td className="payment-amount-cell">
                        {promotionalCode.amount}%
                        <i id={"condition_" + promotionalCode.id} onClick={this.togglePromotionalCodePopover} className="fa fa-lg fa-question-circle promotional-codes-list__question" />
                        <Popover
                            placement="bottom"
                            isOpen={"condition_" + promotionalCode.id === this.state.activePopoverId}
                            target={"condition_" + promotionalCode.id}
                            toggle={this.togglePromotionalCodePopover}
                        >
                            <PopoverBody className="promotional-codes-list__promotional-code-popover-body">
                                <div>
                                    <h3 className="h3-text">Условия скидки</h3>
                                    <div>
                                        {promotionalCode.conditions}
                                    </div>
                                    <a href={promotionalCode.url} className="btn promotional-codes-list__button">Перейти на сайт магазина</a>
                                </div>
                            </PopoverBody>
                        </Popover>
                    </td>
                    <td className="payment-method-cell">{promotionalCode.shop}</td>
                    <td className={classnames("payment-method-cell", this.getStatusClass(promotionalCode))}>{this.getStatus(promotionalCode)}</td>
                </tr>
            )
        });

        const yearSelector = (
            <div className="text-center">
                <select id="years" onChange={this.handleYearChange} value={activeYear}>
                    {years.map((year) => {
                        return (<option key={year} value={year}>{year} год</option>);
                    })}
                </select>
            </div>
        );

        const tableHeader = (
            <thead>
                <tr>
                    <th>Промокод</th>
                    <th>Дата покупки</th>
                    <th>Скидка</th>
                    <th>Магазин</th>
                    <th>Статус</th>
                </tr>
            </thead>
        );

        return (
            <div className="tab-pane active">
                {yearSelector}
                <div>
                    <table className="table table-hover table-responsive-sm payments-table">
                        {tableHeader}
                        <tbody>
                            {items}
                        </tbody>
                    </table>
                </div>
                <Pager
                    onActivePageChange={onActivePageChange}
                    itemsCount={promotionalCodes.length}
                    activePage={activePage}
                    itemsPerPage={itemsPerPage}
                />
            </div>
        );
    }
}
