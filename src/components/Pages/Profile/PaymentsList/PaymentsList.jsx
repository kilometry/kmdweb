import React from 'react';
import moment from 'moment';
import './paymentsList.css';
import { PaymentStatus } from '../../../../stores/Payment';
import Pager, { getPageItems } from '../../Pager/Pager';
import { getPaymentStatusName, getPaymentMethodName } from '../../../../common/payments';

/**
 * Returns the right CSS class depending on the payment status.
 */
function getPaymentIconClass(status) {
    switch (status) {
        case PaymentStatus.Successful:
            return 'fa-check-circle';

        case PaymentStatus.Rejected:
            return 'fa-exclamation-circle';

        case PaymentStatus.Processing:
            return 'fa-question-circle';

        case PaymentStatus.Cancelled:
            return 'fa-ban';

        case PaymentStatus.Created:
            return 'fa-asterisk';

        default:
            throw new Error(`Unknown payment status "${status}"`);
    }
}

/**
 * Returns the right CSS class depending on the payment status.
 */
function getPaymentStatusCellClass(status) {
    switch (status) {
        case PaymentStatus.Successful:
            return 'payment-status-cell--success';

        case PaymentStatus.Rejected:
            return 'payment-status-cell--error';

        case PaymentStatus.Processing:
            return 'payment-status-cell--in-progress';

        case PaymentStatus.Cancelled:
            return 'payment-status-cell--cancelled';

        case PaymentStatus.Created:
            return 'payment-status-cell--created';

        default:
            throw new Error(`Unknown payment status "${status}"`);
    }
}

const PaymentStatusIcon = ({ status }) => (
    <span className={getPaymentStatusCellClass(status)}>
        <i
            className={`fa fa-lg ${getPaymentIconClass(status)}`}
            data-toggle="tooltip"
            data-placement="right"
            title={getPaymentStatusName(status)}
        />
    </span>
);

/**
 * Displays a list of a benefactor payments
 *
 * @export
 * @class PaymentsList
 * @extends {React.Component}
 */
export default class PaymentsList extends React.Component {
    /**
     * fires the event to notify about selected year change.
     */
    handleYearChange = event => {
        this.props.onYearChange(event.target.value);
    }

    render() {
        const { activeYear, years, payments } = this.props;
        const { activePage, itemsPerPage, onActivePageChange } = this.props;
        const items = getPageItems(payments, activePage, itemsPerPage).map(payment => (
            <tr key={payment.id}>
                <td className="payment-code-cell">{payment.id}</td>
                <td className="payment-date-cell">{moment(payment.date).format('DD/MM/YYYY')}</td>
                <td className="payment-amount-cell">{payment.amount}</td>
                <td className="payment-method-cell">{getPaymentMethodName(payment.method)}</td>
                <td className="payment-status-cell">
                    <PaymentStatusIcon status={payment.status} />
                </td>
            </tr>
        ));

        const yearSelector = (
            <div className="text-center">
                <select id="years" onChange={this.handleYearChange} value={activeYear}>
                    {years.map(year => (<option key={year} value={year}>{year} год</option>))}
                </select>
            </div>
        );

        const tableHeader = (
            <thead>
                <tr>
                    <th>Номер</th>
                    <th>Дата</th>
                    <th>Сумма</th>
                    <th>Способ оплаты</th>
                    <th>Статус</th>
                </tr>
            </thead>
        );

        return (
            <div className="tab-pane active">
                {yearSelector}
                <div>
                    <table className="table table-hover table-responsive-sm payments-table">
                        {tableHeader}
                        <tbody>
                            {items}
                        </tbody>
                    </table>
                </div>
                <Pager
                    onActivePageChange={onActivePageChange}
                    itemsCount={payments.length}
                    activePage={activePage}
                    itemsPerPage={itemsPerPage}
                />
            </div>
        );
    }
}
