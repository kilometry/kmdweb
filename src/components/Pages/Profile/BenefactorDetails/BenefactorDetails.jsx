import React from 'react';
import moment from 'moment';
import ajax from '../../../../utils/ajax';

import PaymentsList from '../PaymentsList/PaymentsList';
import PromotionalCodesList from '../PromotionalCodesList/PromotionalCodesList';
import ReportsList from '../ReportsList/ReportsList';

import Tabs from '../Tabs/Tabs';
import Tab from '../Tabs/Tab';
import { connect } from '../../../../utils/flux-utils';
import PaymentStore from '../../../../stores/Payment/PaymentStore';
import PaymentActions from '../../../../stores/Payment/PaymentActions';

class BenefactorDetails extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            reports: [],
            promotionalCodes: [],
            itemsPerPage: 10,
            paymentsActiveYear: moment().year(),
            promotionalCodesActiveYear: moment().year(),
            reportsActiveYear: moment().year(),
            paymentsActivePage: 0,
            reportsActivePage: 0,
            promotionalCodesActivePage: 0
        };
    }

    componentWillMount() {
        PaymentActions.getPayments();

        // get the list of payments
        ajax.getJson('/profile/reports-list.json', true).then(list => {
            this.setState({
                reports: list.reports,
            });
        });

        // get the list of payments
        ajax.getJson('/profile/promotionalCodes-list.json', true).then(list => {
            this.setState({
                promotionalCodes: list.promotionalCodes,
            });
        });
    }

    handleYearChange = listName => {
        if (listName === 'payments') {
            return year => {
                this.setState({
                    paymentsActiveYear: parseInt(year, 10),
                    paymentsActivePage: 0
                });
            };
        } else if (listName === 'promotionalCodes') {
            return year => {
                this.setState({
                    promotionalCodesActiveYear: parseInt(year, 10),
                    promotionalCodesActivePage: 0
                });
            };
        } else if (listName === 'reports') {
            return year => {
                this.setState({
                    reportsActiveYear: parseInt(year, 10),
                    reportsActivePage: 0
                });
            };
        }
    }

    /**
     * Request the selected report for download
     *
     * @memberof Profile
     */
    handleReportClick = report => {
        // / TODO: request the Word document generation from the server
        console.log(report);
    }

    /**
     * Updates the specified param in the state with the new page number.
     *
     */
    handleActivePageChange = stateParam => page => {
        this.setState({ [stateParam]: page });
    }

    /**
     * Gets the list of years available for objects.
     * Each object should have the date field.
     *
     * @param {any} objects List of objects with the date field
     * @returns {Array} list of years available
     * @memberof Profile
     */
    getYearsList(objects) {
        const yearsList = [];

        objects.forEach(obj => {
            const year = moment(obj.date).year();

            if (yearsList.indexOf(year) === -1) {
                yearsList.push(year);
            }
        });

        // sort years in descending order
        return yearsList.sort((a, b) => b - a);
    }

    render() {
        const { match, payments } = this.props;
        const { reports, promotionalCodes } = this.state;
        const { paymentsActiveYear, promotionalCodesActiveYear, reportsActiveYear } = this.state;
        const { paymentsActivePage, reportsActivePage, promotionalCodesActivePage, itemsPerPage } = this.state;

        const yearPayments = payments.filter(payment => moment(payment.date).year() === paymentsActiveYear);
        const yearReports = reports.filter(report => moment(report.date).year() === reportsActiveYear);
        const yearPromotionalCodes = promotionalCodes.filter(promotionalCode => moment(promotionalCode.date).year() === promotionalCodesActiveYear);

        return (
            <Tabs match={match}>
                <Tab title="Оплаты" route="/payments" isDefault>
                    <PaymentsList
                        payments={yearPayments}
                        years={this.getYearsList(payments)}
                        activeYear={paymentsActiveYear}
                        onYearChange={this.handleYearChange('payments')}
                        itemsPerPage={itemsPerPage}
                        activePage={paymentsActivePage}
                        onActivePageChange={this.handleActivePageChange('paymentsActivePage')}
                    />
                </Tab>
                <Tab title="Промокоды" route="/promotional-codes">
                    <PromotionalCodesList
                        promotionalCodes={yearPromotionalCodes}
                        years={this.getYearsList(promotionalCodes)}
                        activeYear={promotionalCodesActiveYear}
                        onYearChange={this.handleYearChange('promotionalCodes')}
                        itemsPerPage={itemsPerPage}
                        activePage={promotionalCodesActivePage}
                        onActivePageChange={this.handleActivePageChange('promotionalCodesActivePage')}
                    />
                </Tab>
                <Tab title="Отчеты" route="/reports">
                    <ReportsList
                        reports={yearReports}
                        years={this.getYearsList(reports)}
                        activeYear={reportsActiveYear}
                        onYearChange={this.handleYearChange('reports')}
                        onReportClick={this.handleReportClick}
                        itemsPerPage={itemsPerPage}
                        activePage={reportsActivePage}
                        onActivePageChange={this.handleActivePageChange('reportsActivePage')}
                    />
                </Tab>
            </Tabs>
        );
    }
}

export default connect(BenefactorDetails, [PaymentStore], (prev, props) => ({
    ...PaymentStore.getState(),
    ...props
}));
