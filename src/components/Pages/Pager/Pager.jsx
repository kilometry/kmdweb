import React from 'react';
import classnames from 'classnames';

import './pager.css';

/**
 * Utility function that returns items for the specified page.
 *
 * @export
 * @param {any} items
 * @param {any} activePage
 * @param {any} itemsPerPage
 * @returns
 */
export function getPageItems(items, activePage, itemsPerPage) {
    const pageItems = [];
    const maxPages = Math.ceil(items / itemsPerPage);

    if (activePage < 0 || activePage > maxPages) {
        return pageItems;
    }

    const startIndex = activePage * itemsPerPage;
    const endIndex = Math.min((activePage + 1) * itemsPerPage, items.length);

    for (let i = startIndex; i < endIndex; i++) {
        pageItems.push(items[i]);
    }

    return pageItems;
}

/**
 * Provides pagination logic for components.
 * activePage property is zero based (i.e. starts with 0).
 *
 * @export
 * @class Pager
 * @extends {React.Component}
 */
export default class Pager extends React.Component {

    handleActivePageClick = (event) => {
        const {onActivePageChange} = this.props;
        const newActivePage = parseInt(event.target.textContent, 10) - 1;

        onActivePageChange(newActivePage);
    }

    render() {
        const {itemsCount, activePage, itemsPerPage} = this.props;

        const pagesCount = Math.ceil(itemsCount / itemsPerPage);

        const pagesItems = [];

        // show pager only if there are more than one page
        if (pagesCount > 1) {
            for (let i = 0; i < pagesCount; i++) {
                pagesItems.push(
                    <li key={i} className="pager__page-item">
                        <a
                            color="link"
                            className={classnames("pager__page-link", {"pager__page-link--active": (i === activePage)})}
                            onClick={(i !== activePage)?this.handleActivePageClick:null}
                        >
                            {i + 1}
                        </a>
                    </li>
                );
            }
        }

        return (
            <ul className="pager">
                { pagesItems }
            </ul>
        );
    }
}
