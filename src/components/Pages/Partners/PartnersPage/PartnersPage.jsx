import React from 'react';
import PropTypes from 'prop-types';
import PageHeader from '../../PageHeader/PageHeader';
import ContactUsForm from '../ContactUsForm/ContactUsForm';
import PartnersList from '../PartnersList/PartnersList';
import content from '../../../../content/partners-content.json';
import partnersList from '../../../../content/partners-list.json';
import LoginStore from '../../../../stores/Login/LoginStore';
import { connect } from '../../../../utils/flux-utils';
import './partners.css';
import '../../typography.css';

const PartnersPage = ({ currentUser }) => (
    <div>
        <PageHeader title={content.title} subTitles={content.subTitles} />
        <ContactUsForm title={content.contactUsFormTitle} currentUser={currentUser} />
        <PartnersList title={content.partnersListTitle} partners={partnersList.partners} />
    </div>
);

PartnersPage.propTypes = {
    currentUser: PropTypes.object.isRequired
};

export default connect(
    PartnersPage,
    [LoginStore],
    (prev, props) => ({
        currentUser: LoginStore.getState().currentUser,
        ...props
    })
);
