import React from 'react';
import PropTypes from 'prop-types';
import asyncConfig from './../../../../config';
import ajax from './../../../../utils/ajax';
import { FormControl } from '../../../form';
import { extractErrorsFromResponse, getErrorMessages } from '../../../../common/errors';

import './contactUsForm.css';
import '../../typography.css';
import logger from '../../../../utils/logger';

export default class ContactUsForm extends React.Component {
    static propTypes = {
        title: PropTypes.string.isRequired,
        currentUser: PropTypes.object.isRequired
    };

    constructor(props) {
        super(props);

        this.state = {
            name: '',
            email: '',
            text: '',
            sendCopy: false,
            errors: {}
        };

        if (props.currentUser.authenticated) {
            this.state.email = props.currentUser.email;
            this.state.name = `${props.currentUser.firstName} ${props.currentUser.lastName}`.trim();
        }
    }

    _handleChange = event => {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.id;

        this.setState({
            [name]: value
        });
    };

    async _handleSubmit() {
        try {
            const config = await asyncConfig;
            await ajax.postJson(`${config.apiEndpoint}/feedback`, this.state);
            this.setState({ text: '', sendCopy: false, errors: {} });
        } catch (errorResponse) {
            logger.error(errorResponse);
            const errors = extractErrorsFromResponse(errorResponse);
            this.setState({ errors });
        }
    }

    render() {
        return (
            <div className="contact-us-form">
                <h3 className="h3-text">{this.props.title}</h3>

                <form>
                    <div className="form-row">
                        <div className="col-lg contact-us-form__column--first">
                            <FormControl
                                label="Ваше имя*:"
                                type="text"
                                id="name"
                                value={this.state.name}
                                onChange={this._handleChange}
                                errors={getErrorMessages(this.state.errors, 'name')}
                            />
                        </div>
                        <div className="col-lg ">
                            <FormControl
                                label="Email"
                                type="email"
                                id="email"
                                value={this.state.email}
                                onChange={this._handleChange}
                                errors={getErrorMessages(this.state.errors, 'email')}
                            />
                        </div>
                    </div>
                    <div className="form-group">
                        <FormControl
                            label="Ваш вопрос"
                            errors={getErrorMessages(this.state.errors, 'text')}
                            renderControl={props => (
                                <textarea
                                    id="text"
                                    rows="6"
                                    value={this.state.text}
                                    onChange={this._handleChange}
                                    {...props}
                                />
                            )}
                        />
                    </div>
                    <div className="form-row">
                        <div className="form-group col-lg">
                            <div className="form-check">
                                <input
                                    id="sendCopy"
                                    className="form-check-input contact-us-form__check-input"
                                    type="checkbox"
                                    value={this.state.sendCopy}
                                    onChange={this._handleChange}
                                />

                                <label className="form-check-label" htmlFor="sendCopy">
                                    Выслать мне копию на почту
                                </label>
                            </div>
                        </div>
                        <div className="form-group text-right col-lg">
                            <button
                                id="submit"
                                type="button"
                                className="btn btn-primary"
                                onClick={() => this._handleSubmit()}
                            >
                                Отправить
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        );
    }
}
