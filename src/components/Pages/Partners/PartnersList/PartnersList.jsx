import React from 'react';

require('./partnersList.css');
require('../../typography.css');

const PartnersList = ({ title, partners }) => (
    <div className="partners-list">
        <div className="partners-list__header">
            <h2 className="h2-text">
                { title }
            </h2>
        </div>

        <div className="partners-list__cards-container">
            { partners.map((partner, index) => {
                return (
                    <div className="partners-list__card" key={index}>
                        <a href={partner.url} className="partners-list__card-link">
                            <div className="partners-list__image-container">
                                <img
                                    src={ process.env.PUBLIC_URL + '/partners/' + partner.image }
                                    className="partners-list__card-image"
                                    alt={partner.name}
                                />
                            </div>
                            <div className="partners-list__content-container">
                                <div className="partners-list__card-name">
                                    { partner.name }
                                </div>
                                <div className="partners-list__card-role">
                                    { partner.description }
                                </div>
                            </div>
                        </a>
                    </div>
                );
            }) }
        </div>
    </div>
);

export default PartnersList;
