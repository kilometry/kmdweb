import React from 'react';
import AgreementContent from './AgreementContent';
import PageHeader from '../PageHeader/PageHeader';
import './BenefactorAgreement.css';

export default () => (
    <div className="agreement-page">
        <PageHeader title="Договор комиссии" />
        <AgreementContent />
    </div>
);
