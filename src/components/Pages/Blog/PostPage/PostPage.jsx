import React from 'react';
import PageHeader from '../../PageHeader/PageHeader';
import ajax from '../../../../utils/ajax';
import moment from 'moment';
import 'moment/locale/ru';

require('./postPage.css');
require('../../typography.css');

export default class PostPage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            post: null,
            htmlContent: "",
            isLoading: true
        };
    }

    componentWillMount() {
        // get target post id from the react router params
        const targetPostId = this.props.match.params.postId;
        this.getPost(targetPostId);
    }

    /**
     * Gets the post metadata and sets it to the state of the component.
     *
     * @memberof PostPage
     */
    getPost = (targetPostId) => {
        // get list of all posts
        ajax.getJson("/posts/posts-list.json", false).then((list) => {
            const posts = list.posts;

            // find the post to show
            for (let i = 0; i < posts.length; i++) {
                const postId = posts[i].content.replace(".html", "");

                if (postId === targetPostId) {
                    // set found post as active
                    this.setState({post: posts[i], isLoading: false}, () => {
                        // load the content of the post
                        this.getPostHtml(posts[i].content);
                    });

                    return;
                }
            }

            // post not found, mark page as loaded
            this.setState({isLoading: false});
        });
    }

    /**
     * Loads the html content of the post and sets it to the state of the component.
     * Name of the html file to load is specified in the 'content' field in the metadata of the target post.
     *
     * @memberof PostPage
     */
    getPostHtml = (fileName) => {
        fetch("/posts/" + fileName, {
            method: 'GET'
        }).then((response) => {
            response.text().then((body) => {
                this.setState({htmlContent: this.fixImagesUrl(body)});
            })
        })
    }

    /**
     * change images urls to point to the folder with posts.
     * A post html may contain images that point to the same folder as html file.
     * To display it correctly, we need to prefix the image path with "posts/".
     *
     * @memberof PostPage
     */
    fixImagesUrl = (content) => {
        return content.replace("<img src=\"", "<img src=\"posts/");
    }

    render() {
        if (this.state.isLoading) {
            return (
                <div className="post-page">
                    <PageHeader title="" subTitles={[]} />
                </div>
            );
        } else {
            let article;
            let title = (this.state.post === null)? ("Новость не найдена") : (this.state.post.title);

            if (this.state.post === null) {
                article = (
                    <article className="post-page__article">
                        Новость не найдена
                    </article>
                );
            } else {
                // localize and format the post's date
                moment().locale("ru");
                const date = moment(this.state.post.date, "DD/MM/YYYY").format("D MMMM, YYYY");

                article = (
                <article className="post-page__article">
                    <div className="post-page__article-date">
                        {date}
                    </div>

                    <div className="post-page__article-content" dangerouslySetInnerHTML={{__html: this.state.htmlContent}}>
                    </div>
                </article>
                );

            }

            return (
                <div className="post-page">
                    <PageHeader title={title} subTitles={[]} />

                    {article}
                </div>
            );
        }
    }
}
