import React from 'react';
import PageHeader from '../../PageHeader/PageHeader';
import PostsList from '../PostsList/PostsList';

import content from '../../../../content/blog-content.json';
import ajax from '../../../../utils/ajax';

export default class BlogPage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            postsList: []
        };
    }

    componentWillMount() {
        ajax.getJson('/posts/posts-list.json', false).then(list => {
            this.setState({ postsList: list.posts });
        });
    }

    render() {
        return (
            <div className="children-page">
                <PageHeader title={content.title} subTitles={content.subTitles} />

                <PostsList posts={this.state.postsList} readMoreText={content.readMore} />
            </div>
        );
    }
}
