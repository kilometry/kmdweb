/* eslint-disable react/no-danger */

import React from 'react';
import PropTypes from 'prop-types';
import './postsList.css';
import '../../typography.css';

const processUrl = url => url.replace('%PUBLIC_URL%', process.env.PUBLIC_URL);

const PostsList = ({ posts, readMoreText }) => (
    <div className="posts-list">
        {posts.map(post => (
            <div className="post-card" key={post.title}>
                <div className="post-card__image-container">
                    {post.image ? (
                        <a href={post.url} className="a-text" rel="noopener noreferrer" target="_blank">
                            <img className="post-card__image" src={processUrl(post.image)} alt={post.title} />
                        </a>
                    ) : null}
                </div>
                <div dangerouslySetInnerHTML={{ __html: post.html }} />
                <div className="post-card__content-container">
                    <h2 className="post-card__title h2-text">{post.title}</h2>
                    <div className="post-card__description">{post.description}</div>
                    {post.url ? (
                        <div className="post-card__read-more">
                            <a href={post.url} className="a-text" rel="noopener noreferrer" target="_blank">
                                {readMoreText}
                            </a>
                        </div>
                    ) : null}
                </div>
            </div>
        ))}
    </div>
);

PostsList.propTypes = {
    posts: PropTypes.array.isRequired,
    readMoreText: PropTypes.string.isRequired
};

export default PostsList;
