import React from 'react';
import { Modal, ModalHeader, ModalBody } from 'reactstrap';
import { getPlural, getNoun } from '../../../../utils/plural';

require('./childDetails.css');
require('../../typography.css');

export default class ChildDetails extends React.Component {
    render() {
        let { isOpen, toggle, child, title } = this.props;

        return (
            <Modal
                isOpen={isOpen}
                className="modal-custom"
                toggle={toggle}
                size="xl"
                autoFocus={false}
            >
                <ModalHeader toggle={toggle} />
                <ModalBody>
                    <div>
                        <h1 className="h1-text">
                            {title}
                </h1>
                        <div className="child-details__container">
                            <div className="child-details__image-container">
                                <img className="child-details__card-image" src={process.env.PUBLIC_URL + '/children/' + this.props.child.image} alt={this.props.child.name} />
                            </div>
                            <div className="child-details__content-container">
                                <div>
                                    <div className="h2-text">
                                        {child.name}
                                    </div>
                                    <div className="p-text">
                                        {child.description}
                                    </div>
                                </div>
                                <div className="child-details__button" onClick={toggle}>
                                    {getNoun(child.sex === "male", "Проехал", "Проехала")}
                                    {` ${child.distance} `}
                                    {getPlural(child.distance, "километр", "километра", "километров")}
                                </div>
                            </div>
                        </div>
                    </div>
                </ModalBody>
            </Modal>
        );
    }
}
