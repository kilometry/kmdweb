import React from 'react';
import PageHeader from '../../PageHeader/PageHeader';
import ChildrenList from '../ChildrenList/ChildrenList';
import ChildDetails from '../ChildDetails/ChildDetails';

import content from '../../../../content/children-content.json';
import ajax from '../../../../utils/ajax';

require('./childrenPage.css');
require('../../typography.css');

export default class ChildrenPage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            selectedChild: null,
            showSelectedChild: false,
            childrenList: []
        };
    }

    componentWillMount() {
        // get the list of children
        ajax.getJson("/children/children-list.json", false).then((list) => {
            this.setState({childrenList: list.children});
        });
    }

    handleToggle = () => {
        this.setState({ showSelectedChild: false });
    }

    handleChildClick = (child) => {
        this.setState(
            {
                selectedChild: child,
                showSelectedChild: true
            });
    }

    render() {
        return (
            <div className="children-page">
                <PageHeader title={content.title} subTitles={content.subTitles} />
                <ChildrenList title={content.childrenListTitle} children={this.state.childrenList} onChildClick={this.handleChildClick} />
                {(this.state.selectedChild !== null) ? (<ChildDetails
                    isOpen={this.state.showSelectedChild}
                    toggle={this.handleToggle}
                    child={this.state.selectedChild}
                    title={content.childDetailsTitle}
                />) : (<div></div>)}
            </div>
        );
    }

}
