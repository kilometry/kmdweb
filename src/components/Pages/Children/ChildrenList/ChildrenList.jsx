import React from 'react';

require('./childrenList.css');
require('../../typography.css');

const ChildrenList = ({ title, children, onChildClick }) => (
    <div className="children-list">
        <div className="children-list__header">
            <h2 className="h2-text">
                {title}
            </h2>
        </div>

        <div className="children-list__cards-container">
            {children.map((child, index) => {
                return (
                    <div className="children-list__card" key={index} onClick={() => {onChildClick(child)}}>
                        <div className="children-list__card-image-container">
                            <img className="children-list__card-image" src={process.env.PUBLIC_URL + '/children/' + child.image} alt={child.name} />
                        </div>
                        <div className="children-list__card-content-container">
                            <div className="children-list__card-name">
                                {child.name}
                            </div>
                            <div className="children-list__card-description">
                                <p>
                                    {child.description}
                                </p>
                            </div>
                            <button type="button" className="btn btn-primary">Смотреть профиль</button>
                        </div>
                    </div>
                );
            })}
        </div>
    </div>
);

export default ChildrenList;
