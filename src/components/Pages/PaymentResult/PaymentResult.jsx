import React from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router';
import { Link } from 'react-router-dom';
import PageHeader from './../PageHeader/PageHeader';
import { PaymentActions, PaymentStatus, PaymentStore } from '../../../stores/Payment';
import { connect } from '../../../utils/flux-utils';
import { getPaymentMethodName, getPaymentStatusName } from '../../../common/payments';
import './paymentResult.css';

function extractNavigationParameters(location) {
    if (!location.search) {
        return {};
    }

    return location.search
        .substring(1)
        .split('&')
        .reduce((state, pair) => {
            const [key, value] = pair.split('=');
            state[key] = value;
            return state;
        }, {});
}

function initializeCurrentPayment(location) {
    const parameters = extractNavigationParameters(location);
    if (parameters['payment-id']) {
        PaymentActions.initializeCurrentPayment(parameters['payment-id']);
    }
}

class PaymentResult extends React.Component {
    static propTypes = {
        location: PropTypes.object.isRequired,
        payment: PropTypes.object
    };

    static defaultProps = {
        payment: null
    }

    componentWillMount() {
        initializeCurrentPayment(this.props.location);
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.location !== nextProps.location) {
            initializeCurrentPayment(nextProps.location);
        }
    }

    _allowStatusCheck(payment) {
        return payment.status === PaymentStatus.Created || payment.status === PaymentStatus.Processing;
    }

    _renderCheckStatusButton(payment) {
        return (
            <input
                type="button"
                className="btn btn-primary payment-result__payment-details__check-status"
                value="Проверить статус оплаты"
                onClick={() => PaymentActions.updateCurrentPayment(payment.id)}
            />
        );
    }

    render() {
        const content = this.props.payment && this.props.payment.loaded && (
            <div className="payment-result__payment-details">
                <div className="payment-result__payment-details__row">
                    <span>Сумма:</span>
                    <span>{`${this.props.payment.amount} ${this.props.payment.currency}`}</span>
                </div>

                <div className="payment-result__payment-details__row">
                    <span>Способ оплаты:</span>
                    <span>{getPaymentMethodName(this.props.payment.method)}</span>
                </div>

                <div className="payment-result__payment-details__row">
                    <span>Статус:</span>
                    <span>{getPaymentStatusName(this.props.payment.status)}</span>
                </div>

                <div className="payment-result__payment-details__row">
                    {this._allowStatusCheck(this.props.payment) && this._renderCheckStatusButton(this.props.payment)}
                </div>

                <div className="payment-result__payment-details__row">
                    <Link
                        className="btn btn-link payment-result__payment-details__go-to-payments"
                        to="/profile/payments"
                    >
                        Перейти к списку платежей
                    </Link>
                </div>
            </div>
        );

        return (
            <div>
                <PageHeader title="Платёж совершён" />
                {content}
            </div>
        );
    }
}

export default withRouter(connect(PaymentResult, [PaymentStore], (prev, props) => ({
    payment: PaymentStore.getState().currentPayment,
    ...props
})));
