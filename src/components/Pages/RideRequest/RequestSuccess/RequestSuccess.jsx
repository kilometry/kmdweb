import React from 'react';

import './requestSuccess.css';

const RequestSuccess = ({onNewRequestClick}) => (
    <div className="request-success">
        <h2 className="h2-text">
            Поездка зарегистрирована. Спасибо!
        </h2>
        <button className="btn btn-primary" onClick={onNewRequestClick}>Оформить новую поездку</button>
    </div>
);

export default RequestSuccess;
