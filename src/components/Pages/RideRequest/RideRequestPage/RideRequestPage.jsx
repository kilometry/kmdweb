import React from 'react';
import PageHeader from '../../PageHeader/PageHeader';
import RequestForm from '../RequestForm/RequestForm';
import RequestError from '../RequestError/RequestError';
import RequestSuccess from '../RequestSuccess/RequestSuccess';

import content from '../../../../content/rideRequest-content.json';

require('./rideRequestPage.css');

const RequestStatus = {
    InProgress: "InProgress",
    Success: "Success",
    Error: "Error"
}

class RideRequestPage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            requestStatus: RequestStatus.InProgress
        };
    }

    handleRequestSubmit = (request) => {
        console.log(request);
        this.setState({requestStatus: RequestStatus.Success});
    }

    handleNewRequestClick = () => {
        this.setState({requestStatus: RequestStatus.InProgress});
    }

    render() {
        const {requestStatus} = this.state;
        let requestView = null;

        if (requestStatus === RequestStatus.InProgress) {
            requestView = (
                <RequestForm
                    conditions={content.conditions}
                    onRequestSubmit={this.handleRequestSubmit}
                />
            );
        } else if (requestStatus === RequestStatus.Error) {
            requestView = (<RequestError errors={this.state.errors} onNewRequestClick={this.handleNewRequestClick} />);
        } else if (requestStatus === RequestStatus.Success) {
            requestView = (<RequestSuccess onNewRequestClick={this.handleNewRequestClick} />);
        }

        return (
            <div>
                <PageHeader title={content.title} subTitles={content.subTitles} />

                {requestView}
            </div>
        );
    }
}

export default RideRequestPage;
