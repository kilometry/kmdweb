import React from 'react';
import { Button, Dropdown, DropdownToggle, DropdownMenu, DropdownItem, Input, Label } from 'reactstrap';
import Calendar from 'react-calendar';

import './requestForm.css';
import '../../typography.css';
import LoginStore from '../../../../stores/Login/LoginStore';
import LoginActions from '../../../../stores/Login/LoginActions';
import { connect } from '../../../../utils/flux-utils';

const RequestTypes = {
    PreSchool: "Дошкольное учреждение",
    Hospital: "Медицинское учреждение",
    PrivateDoctor: "Частный врач",
    School: "Школа",
    Hospice: "Хоспис",
    Other: "Другое"
}

const DefaultRequestType = RequestTypes.PreSchool;

/**
 * Shows the ride request form
 *
 * @export
 * @class RequestForm
 * @extends {React.Component}
 */
class RequestForm extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            type: DefaultRequestType,
            from: "",
            to: "",
            date: new Date(),
            time: "",
            agreedToConditions: true,
            dropdownOpen: false,
            errors: []
        };
    }

    handleChange = (event) => {
        const target = event.target;
        const value = target.type === "checkbox" ? target.checked : target.value;
        const name = target.id;

        this.setState({
            [name]: value
        });
    }

    handleDropdownChange = (event) => {
        this.setState({ type: event.currentTarget.textContent });
    }

    handleDateChange = date => this.setState({date: date});

    toggleDropdown = () => {
        this.setState({
            dropdownOpen: !this.state.dropdownOpen
        });
    }

    handleSubmit = (event) => {
        event.preventDefault();

        if (this.validate()) {
            const { from, to, type, date, time } = this.state;
            this.props.onRequestSubmit({
                from,
                to,
                type,
                date,
                time
            });
        }
    }

    validate = () => {
        const { type, from, to, date, time, agreedToConditions } = this.state;
        const errors = [];

        if (from === "") {
            errors.push("Укажите начальный адрес поездки.");
        }

        if (to === "") {
            errors.push("Укажите конечный адрес поездки.");
        }

        if (type === "") {
            errors.push("Выберите цель поездки.");
        }

        if (date === "") {
            errors.push("Выберите дату поездки.");
        }

        if (time === "") {
            errors.push("Укажите время поездки.");
        }

        if (!agreedToConditions) {
            errors.push("Для продолжения вам надо принять условия поездки.");
        }

        this.setState({ errors: errors });

        if (errors.length !== 0) {
            return false;
        } else {
            return true;
        }
    }

    handleLoginClick = () => {
        LoginActions.showLoginPopup();
    }

    handleRegisterClick = () => {
        LoginActions.showRegisterSponsor();
    }

    render() {
        const { conditions, currentUser } = this.props;
        const { from, to, type, date, time, agreedToConditions, dropdownOpen } = this.state;

        if (!currentUser.authenticated || !LoginStore.isFamily(currentUser)) {
            return (
                <div className="request-form">
                    <div>
                        <div>
                            <h3 className="h3-text">
                                Оплатить поездки
                            </h3>
                        </div>
                        <div className="request-form__login-request">
                            <div>
                                Для того, чтобы заказать поездку, вам нужно войти на сайт или зарегистрироваться как семья с особенным ребенком
                            </div>
                            <div>
                                <button onClick={this.handleLoginClick} className="btn btn-primary">Войти</button>
                            </div>
                            <div>
                                <button onClick={this.handleRegisterClick} className="btn btn-primary">Зарегистрироваться</button>
                            </div>
                        </div>
                    </div>
                </div>
            );
        } else {
            const { errors } = this.state;
            const errorsContent = errors.map((error, index) => (<div key={index}>{error}</div>));
            const errorsPanel = (errors.length !== 0) ? (<div className="alert alert-danger">{errorsContent}</div>) : (null);

            return (
                <div className="request-form">
                    {errorsPanel}
                    <div className="request-form__container">
                        <div>
                            <h3 className="h3-text">
                                Оформить поездку
                            </h3>
                            <form>
                                <div className="form-group">
                                    <Label for="from">Адрес, откуда вас забрать:</Label>
                                    <Input
                                        type="text"
                                        name="from"
                                        id="from"
                                        value={from}
                                        onChange={this.handleChange}
                                    />
                                </div>
                                <div className="form-group">
                                    <Label for="to">Адрес, куда вас отвезти:</Label>
                                    <Input
                                        type="text"
                                        name="to"
                                        id="to"
                                        value={to}
                                        onChange={this.handleChange}
                                    />
                                </div>
                                <div className="form-group">
                                    <label htmlFor="name">Цель поездки:</label>
                                    <Dropdown
                                        id="type"
                                        isOpen={dropdownOpen}
                                        toggle={this.toggleDropdown}
                                    >
                                        <DropdownToggle caret className="request-form__dropdown">
                                            {type}
                                        </DropdownToggle>
                                        <DropdownMenu className="request-form__dropdown-menu">
                                            <DropdownItem className="request-form__dropdown-menu-item" onClick={this.handleDropdownChange}>{RequestTypes.PreSchool}</DropdownItem>
                                            <DropdownItem className="request-form__dropdown-menu-item" onClick={this.handleDropdownChange}>{RequestTypes.Hospital}</DropdownItem>
                                            <DropdownItem className="request-form__dropdown-menu-item" onClick={this.handleDropdownChange}>{RequestTypes.PrivateDoctor}</DropdownItem>
                                            <DropdownItem className="request-form__dropdown-menu-item" onClick={this.handleDropdownChange}>{RequestTypes.School}</DropdownItem>
                                            <DropdownItem className="request-form__dropdown-menu-item" onClick={this.handleDropdownChange}>{RequestTypes.Hospice}</DropdownItem>
                                            <DropdownItem className="request-form__dropdown-menu-item" onClick={this.handleDropdownChange}>{RequestTypes.Other}</DropdownItem>
                                        </DropdownMenu>
                                    </Dropdown>
                                </div>
                                <div className="form-group">
                                    <Label for="date">День поездки:</Label>
                                    <Calendar
                                        onChange={this.handleDateChange}
                                        value={date}
                                        locale="ru"
                                        maxDetail="month"
                                        minDetail="month"
                                        className="request-form__calendar"
                                    />
                                </div>
                                <div className="form-group">
                                    <Label for="time">Время, в которое вас забрать:</Label>
                                    <Input
                                        type="time"
                                        name="time"
                                        id="time"
                                        value={time}
                                        onChange={this.handleChange}
                                    />
                                </div>
                                <div className="form-group text-right">
                                    <Button onClick={this.handleSubmit} className="btn btn-primary">Оформить</Button>
                                </div>
                            </form>
                        </div>
                        <div>
                            <h3 className="h3-text">
                                Условия
                            </h3>
                            <input
                                type="checkbox"
                                id="agreedToConditions"
                                defaultChecked={agreedToConditions}
                                value={agreedToConditions}
                                onChange={this.handleChange}
                            />
                            <label className="form-check-label" htmlFor="agreedToConditions">
                                Я согласен с условиями
                            </label>
                            <div>
                                {conditions}
                            </div>
                        </div>
                    </div>
                </div>
            );
        }
    }
}

export default connect(RequestForm, [LoginStore], (prev, props) => ({
    ...LoginStore.getState(),
    ...props
}));
