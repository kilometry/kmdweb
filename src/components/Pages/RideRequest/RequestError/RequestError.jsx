import React from 'react';

import './requestError.css';

const RequestError = ({errors, onNewRequestClick}) => (
    <div className="request-error">
        <h2 className="h2-text">
            Ошибка при заказе поездки.
        </h2>
        <div className="alert alert-danger">
            {errors.map((error, index) => (<div key={index}>{error}</div>))}
        </div>
        <button className="btn btn-primary" onClick={onNewRequestClick}>Повторить заказ</button>
    </div>
);

export default RequestError;
