/* eslint-disable react/jsx-no-target-blank */

import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import paymentMethods from './paymentMethods.png';
import bePaid from './bePaid.png';
import './pageFooter.css';

const PageFooter = ({ menuItems }) => (
    <footer className="page-footer">
        <div className="page-footer__content">
            <div className="page-footer__links-container">
                <ul className="page-footer__links page-footer__links--menu">
                    {menuItems.map(menuItem => (
                        <li key={menuItem.name}>
                            <Link className="page-footer__link" to={`/${menuItem.to}`}>
                                {menuItem.name}
                            </Link>
                        </li>
                    ))}
                </ul>
            </div>
            <div className="page-footer__links-container">
                <ul className="page-footer__links page-footer__links--social">
                    <li>
                        <a
                            href="https://www.facebook.com/kmdobra"
                            className="page-footer__link page-footer__link--social"
                        >
                            <i className="fa fa-facebook-f" />
                        </a>
                    </li>
                    <li>
                        <a href="https://vk.com/kmdobra" className="page-footer__link page-footer__link--social">
                            <i className="fa fa-vk" />
                        </a>
                    </li>
                    <li>
                        <a href="https://www.instagram.com" className="page-footer__link page-footer__link--social">
                            <i className="fa fa-instagram" />
                        </a>
                    </li>
                </ul>
            </div>
            <div className="page-footer__links-container">
                <ul className="page-footer__links page-footer__links--contact">
                    <li>
                        <span className="page-footer__link page-footer__link--text">ОАО «Километры Добра»</span>
                    </li>
                    <li>
                        <a href="tel:+375 29 694-57-73" className="page-footer__link page-footer__link--contact">
                            +375 29 694-57-73
                        </a>
                    </li>
                    <li>
                        <a href="mailto:dmitry@kilometry.by" className="page-footer__link page-footer__link--contact">
                            dmitry@kilometry.by
                        </a>
                    </li>
                </ul>
            </div>
        </div>

        <div className="page-footer__payment-methods">
            <div className="page-footer__payment-methods__cards">
                <img src={paymentMethods} alt="Сайт поддерживает оплаты банковскими картами." />
            </div>

            <div className="page-footer__payment-methods__bePaid">
                <a href="https://bepaid.by" target="_blank" rel="noopener noreferer">
                    <img src={bePaid} alt="Система оплаты bePaid" />
                </a>
            </div>
        </div>

        <div className="page-footer__hosting">
            Бесплатный хостинг предоставлен{' '}
            <a href="https://hoster.by" target="_blank">
                hoster.by
            </a>
        </div>
    </footer>
);

PageFooter.propTypes = {
    menuItems: PropTypes.array.isRequired
};

export default PageFooter;
