/* eslint-disable react/no-array-index-key */

import React from 'react';
import PropTypes from 'prop-types';
import './pageHeader.css';
import '../typography.css';

const StringItem = ({ text }) => text.split('\n').map((line, key) => (<span key={key}>{line}<br /></span>));

const PageHeader = ({ title, subTitles }) => (
    <div className="page-header page-header--gradient">
        <div className="page-header__content">
            <h1 className="h1-text">
                { title }
            </h1>
            {subTitles.map((subTitle, index) => (
                <h3 className="h3-text" key={index}>
                    {typeof subTitle === 'string' ? <StringItem text={subTitle} /> : subTitle}
                </h3>
            ))}
        </div>
    </div>
);

PageHeader.propTypes = {
    title: PropTypes.string.isRequired,
    subTitles: PropTypes.arrayOf(PropTypes.node)
};

PageHeader.defaultProps = {
    subTitles: []
};

export default PageHeader;
