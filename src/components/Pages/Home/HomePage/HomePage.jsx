import React from 'react';
import AboutProject from '../../About/AboutProject/AboutProject';
import Login from '../Login/Login';
import PromotionsList from '../../Promotions/PromotionsList/PromotionsList';
import PromotionDetails from '../../Promotions/PromotionDetails/PromotionDetails';

import content from '../../../../content/home-content.json';
import ajax from '../../../../utils/ajax';

require('./homePage.css');
require('../../typography.css');

export default class HomePage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            selectedPromotion: null,
            showModal: false,
            promotionsList: []
        };
    }

    componentWillMount() {
        // get the list of promotions
        ajax.getJson('/promotions/promotions-list.json', false).then(list => {
            this.setState({ promotionsList: list.promotions });
        });
    }

    /**
     * Gets the list of promotions filtered by the active category
     *
     * @memberof PromotionsPage
     */
    getPromotions = promotions => promotions.slice(0, 4);

    /**
     * Changes the state to close the modal window
     *
     * @memberof PromotionsPage
     */
    handleToggle = () => {
        this.setState({ showModal: false });
    };

    /**
     * Sets the selected promotion in the state and opens the modal window
     *
     * @memberof PromotionsPage
     */
    handlePromotionClick = promotion => {
        this.setState({
            selectedPromotion: promotion,
            showModal: true
        });
    };

    render() {
        const promotionDetails =
            this.state.selectedPromotion !== null ? (
                <PromotionDetails
                    isOpen={this.state.showModal}
                    toggle={this.handleToggle}
                    promotion={this.state.selectedPromotion}
                    title={content.promotionTitle}
                />
            ) : (
                <div />
            );

        return (
            <div className="home-page">
                <Login
                    title={content.loginTitle}
                    distance={content.loginDistance}
                    loginRequest={content.loginRequest}
                />
                <div className="home-page__promotions-title">
                    <h2 className="h2-text">{content.promotionsTitle}</h2>
                </div>

                <PromotionsList
                    promotions={this.getPromotions(this.state.promotionsList)}
                    onPromotionClick={this.handlePromotionClick}
                />

                <AboutProject
                    title={content.aboutTitle}
                    header={content.aboutHeader}
                    content={content.about}
                    showMoreInfo
                />

                {promotionDetails}
            </div>
        );
    }
}
