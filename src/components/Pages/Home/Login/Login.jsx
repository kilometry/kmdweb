/* eslint-disable react/no-array-index-key */
import React from 'react';
import PropTypes from 'prop-types';
import LoginActions from '../../../../stores/Login/LoginActions';
import LoginStore from '../../../../stores/Login/LoginStore';
import { connect } from '../../../../utils/flux-utils';
import { translateErrors } from '../../../../utils/errorsTranslations';
import './login.css';
import '../../typography.css';

class Login extends React.Component {
    static propTypes = {
        currentUser: PropTypes.object.isRequired,
        title: PropTypes.string.isRequired,
        distance: PropTypes.string.isRequired,
        loginRequest: PropTypes.string.isRequired,
        errors: PropTypes.object.isRequired
    };

    state = {
        email: '',
        password: '',
        errors: []
    };

    /**
     * Updates values of input fields in the state.
     *
     * @memberof Login
     */
    handleChange = event => {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.id;

        this.setState({
            [name]: value
        });
    };

    /**
     * Handles login button click by firing login action.
     *
     * @memberof Login
     */
    handleSubmit = event => {
        event.preventDefault();

        if (this.validate()) {
            const { email, password } = this.state;
            LoginActions.login(email, password);
        }
    };

    validate() {
        const errors = [];

        const { email, password } = this.state;

        if (email === '') {
            errors.push('Укажите адрес электронной почты для входа.');
        }

        if (password === '') {
            errors.push('Укажите пароль для входа.');
        }

        this.setState({ errors });

        if (errors.length > 0) {
            return false;
        }
        return true;
    }

    /**
     * Fires actions to open popup with register information
     *
     * @memberof Login
     */
    handleRegisterClick = registrationType => {
        if (registrationType === 'family') {
            return () => LoginActions.showRegisterFamily();
        } else if (registrationType === 'sponsor') {
            return () => LoginActions.showRegisterSponsor();
        }

        throw new Error(`Unknown registration type '${registrationType}'`);
    };

    render() {
        const { errors } = this.state;
        const errorsFromServer =
            this.props.errors.general !== undefined ? translateErrors(this.props.errors).general : [];
        const allErrors = errors.concat(errorsFromServer);
        const errorsContent = allErrors.map((error, index) => <div key={index}>{error}</div>);
        const errorsPanel =
            allErrors.length !== 0 ? <div className="alert alert-danger text-left">{errorsContent}</div> : null;

        const form = (
            <form>
                <div className="form-row">
                    <div className="col-lg">
                        <input
                            id="email"
                            type="text"
                            className="form-control login__form-field"
                            placeholder="email"
                            value={this.state.email}
                            onChange={this.handleChange}
                        />
                    </div>
                    <div className="col-lg">
                        <input
                            id="password"
                            type="password"
                            className="form-control login__form-field"
                            placeholder="пароль"
                            value={this.state.password}
                            onChange={this.handleChange}
                        />
                    </div>
                    <div className="form-group text-right col-m">
                        <button id="submit" type="submit" className="btn btn-primary" onClick={this.handleSubmit}>
                            Войти
                        </button>
                    </div>
                </div>
            </form>
        );

        const loginRequest = this.props.currentUser.authenticated ? (
            <div>
                <h2>Добро пожаловать, {this.props.currentUser.firstName}.</h2>
            </div>
        ) : (
            <div className="login__login-form">
                <h3 className="h3-text">{this.props.loginRequest}</h3>
                {errorsPanel}
                {form}
                <div className="login__login-request">
                    <h3 className="h3-text">Или зарегистрируйтесь</h3>
                    <div className="login__links-container">
                        <button
                            onClick={this.handleRegisterClick('sponsor')}
                            className="a-text login__link btn btn-link"
                        >
                            как благотворитель
                        </button>
                        <button
                            onClick={this.handleRegisterClick('family')}
                            className="a-text login__link btn btn-link"
                        >
                            как семья с особенным ребенком
                        </button>
                    </div>
                </div>
            </div>
        );

        return (
            <div className="login__container">
                <div className="login__content-container">
                    <h1 className="login__title h1-text">{this.props.title}</h1>
                    <div className="login__distance login__distance-text">{this.props.distance}</div>

                    {loginRequest}
                </div>
            </div>
        );
    }
}

export default connect(
    Login,
    [LoginStore],
    (prev, props) => ({
        ...LoginStore.getState(),
        ...props
    })
);
