import React from 'react';
import LoginActions from '../../../stores/Login/LoginActions';
import PageHeader from './../PageHeader/PageHeader';
import './emailConfirmed.css';

export default () => {
    const content = (
        <div className="email-confirmed-page">
            <div>Ваша учетная запись была активирована.</div>
            <div>
                Теперь вы можете использовать введённые адрес электронной почты
                и пароль, чтобы
                <input
                    type="button"
                    className="btn btn-link email-confirmed-page__login-link"
                    value="войти в систему"
                    onClick={() => LoginActions.showLoginPopup()}
                />.
            </div>
        </div>
    );

    return (
        <div className="email-confirmed-page">
            <PageHeader title="Учетная запись активирована" subTitles={[content]} />
        </div>
    );
};

