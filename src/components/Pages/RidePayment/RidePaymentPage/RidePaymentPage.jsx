import React from 'react';
import PropTypes from 'prop-types';
import PageHeader from '../../PageHeader/PageHeader';
import PaymentForm from '../PaymentForm/PaymentForm';
import PaymentError from '../PaymentError/PaymentError';
import PaymentSuccess from '../PaymentSuccess/PaymentSuccess';

import content from '../../../../content/ridePayment-content.json';
import PaymentActions from '../../../../stores/Payment/PaymentActions';
import { connect } from '../../../../utils/flux-utils';
import PaymentStore from '../../../../stores/Payment/PaymentStore';
import EditingObjectState from '../../../../stores/EditingObjectState';
import Loading from '../../../loading';
import LoginStore from '../../../../stores/Login/LoginStore';
import { createUrlForRoute, Routes } from '../../../../utils/routing';

import './ridePaymentPage.css';

class RidePaymentPage extends React.Component {
    static propTypes = {
        state: PropTypes.string.isRequired,
        history: PropTypes.object.isRequired,
        errors: PropTypes.object.isRequired,
        currentUser: PropTypes.object.isRequired,
        availablePaymentMethods: PropTypes.array.isRequired,
        prerequisitesLoaded: PropTypes.bool.isRequired
    };

    state = {};

    handlePaymentSubmit = async payment => {
        const paymentResultUri = createUrlForRoute(Routes.PAYMENT_RESULT, this.props.history);
        PaymentActions.addNewPayment({ redirectUri: paymentResultUri, ...payment });
    };

    handleNewPaymentClick = () => {
        PaymentActions.createNewPayment();
    };

    componentDidMount() {
        PaymentActions.loadPaymentMethods();
    }

    render() {
        const { state, currentUser, prerequisitesLoaded, availablePaymentMethods } = this.props;
        let paymentView = null;

        if (state === EditingObjectState.Editing || state === EditingObjectState.Saving) {
            paymentView = (
                <PaymentForm
                    paymentMethods={availablePaymentMethods}
                    currentUser={currentUser}
                    onPaymentSubmit={this.handlePaymentSubmit}
                />
            );
        } else if (state === EditingObjectState.Error) {
            paymentView = <PaymentError errors={this.props.errors} onNewPaymentClick={this.handleNewPaymentClick} />;
        } else if (state === EditingObjectState.Success) {
            paymentView = <PaymentSuccess onNewPaymentClick={this.handleNewPaymentClick} />;
        }

        return (
            <div>
                <PageHeader title={content.title} subTitles={content.subTitles} />
                <Loading isVisible={!prerequisitesLoaded || state === EditingObjectState.Saving} />
                {prerequisitesLoaded && paymentView}
            </div>
        );
    }
}

export default connect(
    RidePaymentPage,
    [PaymentStore, LoginStore],
    (prev, props) => ({
        ...PaymentStore.getState().newPayment,
        currentUser: LoginStore.getState().currentUser,
        ...props
    })
);
