/* eslint-disable react/no-array-index-key */

import React from 'react';
import PropTypes from 'prop-types';
import { values, flatten } from 'lodash';
import './paymentError.css';

const PaymentError = ({ errors, onNewPaymentClick }) => {
    let errorsList = errors.fields ? flatten(values(errors.fields)) : [];
    if (errors.general && errors.general.length) {
        errorsList = errorsList.concat(errors.general);
    }

    return (
        <div className="payment-error">
            <h2 className="h2-text">Ошибка при проведении платежа.</h2>
            <div className="alert alert-danger">{errorsList.map((error, index) => <div key={index}>{error}</div>)}</div>
            <button className="btn btn-primary" onClick={onNewPaymentClick}>
                Новый платеж
            </button>
        </div>
    );
};

PaymentError.propTypes = {
    errors: PropTypes.object.isRequired,
    onNewPaymentClick: PropTypes.func.isRequired
};

export default PaymentError;
