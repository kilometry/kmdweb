/* eslint-disable react/no-array-index-key,react/no-did-update-set-state */

import React from 'react';
import { Button, Dropdown, DropdownToggle, DropdownMenu, DropdownItem, Input } from 'reactstrap';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import './paymentForm.css';
import '../../typography.css';
import LoginStore from '../../../../stores/Login/LoginStore';
import LoginActions from '../../../../stores/Login/LoginActions';
import PaymentMethod from '../../../../stores/Payment/PaymentMethod';
import { Routes } from '../../../../utils/routing';

const PaymentMethodsLabels = {
    [PaymentMethod.BePaid]: 'Онлайн оплата через сервис bePaid',
    [PaymentMethod.CashInOffice]: 'Наличными в офисе',
    [PaymentMethod.CashToCourier]: 'Наличными курьеру',
    [PaymentMethod.Ssis]: 'ЕРИП'
};

const PaymentMethodComments = {
    [PaymentMethod.BePaid]: (
        <div>
            <p>bePaid - Прием платежей онлайн по банковским платежным картам VISA, MasterCard, БЕЛКАРТ и Халва</p>
        </div>
    )
};

const PaymentMethodAdditionalInfo = {
    [PaymentMethod.BePaid]: (
        <div>
            <p>
                Для совершения оплаты вы будете перенаправлены на специальную защищенную платежную страницу
                процессинговой системы <a href="https://bepaid.by">bePaid</a>. На платежной странице будет указан номер
                заказа и сумма платежа. Для оплаты вам необходимо ввести свои карточные данные и подтвердить платеж,
                нажав кнопку «Оплатить». Если ваша карта поддерживает технологию 3-D Secure, системой вам будет
                предложено пройти стандартную одноминутную процедуру проверки владельца карты на странице вашего банка.
            </p>
            <p>
                После проведения платежа на указанный вами электронный адрес придет письмо, подтверждающее факт оплаты.
            </p>
            <p className="payment-form__no-refund">
                Обращаем ваше внимание на то, что возврат денежных средств не предусмотрен.
            </p>
        </div>
    )
};

// const DefaultPaymentMethod = PaymentMethod.BePaid;

/**
 * Shows the ride payment form
 *
 * @export
 * @class PaymentForm
 * @extends {React.Component}
 */
class PaymentForm extends React.Component {
    static propTypes = {
        onPaymentSubmit: PropTypes.func.isRequired,
        currentUser: PropTypes.object.isRequired,
        paymentMethods: PropTypes.array.isRequired
    };

    constructor(props) {
        super(props);

        this.state = {
            method: props.paymentMethods.length > 0 ? props.paymentMethods[0] : null,
            amount: '',
            agreedToConditions: false,
            dropdownOpen: false,
            errors: []
        };
    }

    componentDidUpdate() {
        if (
            (!this.state.method || this.props.paymentMethods.indexOf(this.state.method) < 0) &&
            this.props.paymentMethods.length > 0
        ) {
            this.setState({ method: this.props.paymentMethods[0] });
        }
    }

    handleChange = event => {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.id;

        this.setState({
            [name]: value
        });
    };

    handleDropdownChange = event => {
        const textContent = event.currentTarget.textContent;
        let method = null;

        if (textContent === PaymentMethodsLabels[PaymentMethod.BePaid]) {
            method = PaymentMethod.BePaid;
        } else if (textContent === PaymentMethodsLabels[PaymentMethod.CashToCourier]) {
            method = PaymentMethod.CashToCourier;
        } else if (textContent === PaymentMethodsLabels[PaymentMethod.CashInOffice]) {
            method = PaymentMethod.CashInOffice;
        } else {
            throw new Error(`Unknown payment method selected: ${textContent}`);
        }

        this.setState({ method });
    };

    toggleDropdown = () => {
        this.setState({
            dropdownOpen: !this.state.dropdownOpen
        });
    };

    handleSubmit = event => {
        event.preventDefault();

        if (this.validate()) {
            const { method, amount } = this.state;
            this.props.onPaymentSubmit({ method, amount: Math.round(parseFloat(amount) * 100) / 100 });
        }
    };

    validate = () => {
        const { method, amount, agreedToConditions } = this.state;
        const errors = [];

        if (method === '') {
            errors.push('Выберите способ оплаты.');
        }

        if (amount === '') {
            errors.push('Введите сумму для оплаты.');
        }

        if (!agreedToConditions) {
            errors.push('Для продолжения вам надо принять условия оплаты.');
        }

        this.setState({ errors });

        return errors.length === 0;
    };

    handleLoginClick = () => {
        LoginActions.showLoginPopup();
    };

    handleRegisterClick = () => {
        LoginActions.showRegisterSponsor();
    };

    _renderAuthenticationButtons() {
        return (
            <div className="payment-form">
                <div>
                    <div>
                        <h3 className="h3-text">Оплатить поездки</h3>
                    </div>
                    <div className="payment-form__login-request">
                        <div>
                            Для того, чтобы оплатить поездки особенным детям вам нужно войти на сайт или
                            зарегистрироваться как благотворитель.
                        </div>
                        <div>
                            <button onClick={this.handleLoginClick} className="btn btn-primary">
                                Войти
                            </button>
                        </div>
                        <div>
                            <button onClick={this.handleRegisterClick} className="btn btn-primary">
                                Зарегистрироваться
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    _renderPaymentForm() {
        const { errors } = this.state;
        const errorsContent = errors.map((error, index) => <div key={index}>{error}</div>);
        const errorsPanel = errors.length !== 0 ? <div className="alert alert-danger">{errorsContent}</div> : null;

        const { paymentMethods } = this.props;
        const { method, amount, agreedToConditions, dropdownOpen } = this.state;
        const paymentMethodAdditionalInfo = PaymentMethodAdditionalInfo[method];

        return (
            <div className="payment-form">
                {errorsPanel}
                <div className="payment-form__container">
                    <div>
                        <h3 className="h3-text">Оплатить поездки</h3>
                        <form>
                            <div className="form-group">
                                <label htmlFor="method">Способ оплаты:</label>
                                <Dropdown id="method" isOpen={dropdownOpen} toggle={this.toggleDropdown}>
                                    <DropdownToggle caret className="payment-form__dropdown">
                                        {PaymentMethodsLabels[method]}
                                    </DropdownToggle>
                                    <DropdownMenu className="payment-form__dropdown-menu">
                                        {paymentMethods.map(m => (
                                            <DropdownItem
                                                key={method}
                                                className="payment-form__dropdown-menu-item"
                                                onClick={this.handleDropdownChange}
                                            >
                                                {PaymentMethodsLabels[m]}
                                            </DropdownItem>
                                        ))}
                                    </DropdownMenu>
                                </Dropdown>
                                <div className="payment-form__comment">{PaymentMethodComments[method]}</div>
                            </div>
                            <div className="form-group">
                                <label htmlFor="amount">Сумма оплаты:</label>
                                <Input
                                    type="number"
                                    name="amount"
                                    id="amount"
                                    value={amount}
                                    onChange={this.handleChange}
                                />
                            </div>
                            <div className="form-group text-right">
                                <Button onClick={this.handleSubmit} className="btn btn-primary">
                                    Оплатить
                                </Button>
                            </div>
                        </form>
                    </div>
                    <div>
                        <div>
                            {paymentMethodAdditionalInfo ? (
                                <h3 className="h3-text">Дополнительная информация</h3>
                            ) : null}
                            {paymentMethodAdditionalInfo}
                        </div>
                        <h3 className="h3-text">Условия оплаты</h3>
                        <div className="form-check">
                            <input
                                type="checkbox"
                                id="agreedToConditions"
                                defaultChecked={agreedToConditions}
                                value={agreedToConditions}
                                onChange={this.handleChange}
                                className="form-check-input"
                            />
                            <label className="form-check-label" htmlFor="agreedToConditions">
                                Я согласен с условиями представленными в{' '}
                                <Link target="_blank" to={Routes.BENEFACTOR_AGREEMENT}>
                                    Договоре Комиссии
                                </Link>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    render() {
        const currentUser = this.props.currentUser;

        return currentUser.authenticated && LoginStore.isBenefactor(currentUser)
            ? this._renderPaymentForm()
            : this._renderAuthenticationButtons();
    }
}

export default PaymentForm;
