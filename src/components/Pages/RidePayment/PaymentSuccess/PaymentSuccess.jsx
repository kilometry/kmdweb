import React from 'react';
import {Link} from 'react-router-dom';
import './paymentSuccess.css';

const PaymentSuccess = ({onNewPaymentClick}) => (
    <div className="payment-success">
        <h2 className="h2-text">
            Платеж зарегистрирован. Спасибо!
        </h2>
        <button className="btn btn-primary" onClick={onNewPaymentClick}>Новый платеж</button>
        <div>
            <Link className="btn btn-link" to="/profile/payments">Перейти в личный кабинет</Link>
        </div>
    </div>
);

export default PaymentSuccess;
