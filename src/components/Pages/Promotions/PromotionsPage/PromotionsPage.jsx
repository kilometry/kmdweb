import React from 'react';
import PageHeader from '../../PageHeader/PageHeader';
import PromotionsList from '../PromotionsList/PromotionsList';
import PromotionDetails from '../PromotionDetails/PromotionDetails';
import PromotionsCategories from '../PromotionsCategories/PromotionsCategories';

import content from '../../../../content/promotions-content.json';
import ajax from '../../../../utils/ajax';

require('./promotionsPage.css');
require('../../typography.css');

export default class PromotionsPage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            selectedPromotion: null,
            showModal: false,
            promotionsList: [],
            activeCategory: ""
        };
    }

    componentWillMount() {
        // get the list of promotions
        ajax.getJson("/promotions/promotions-list.json", false).then((list) => {
            this.setState({promotionsList: list.promotions});
        });
    }

    /**
     * Changes the state to close the modal window
     *
     * @memberof PromotionsPage
     */
    handleToggle = () => {
        this.setState({ showModal: false });
    }

    /**
     * Sets the selected promotion in the state and opens the modal window
     *
     * @memberof PromotionsPage
     */
    handlePromotionClick = (promotion) => {
        this.setState(
            {
                selectedPromotion: promotion,
                showModal: true
            });
    }

    /**
     * Sets the selected category as active
     *
     * @memberof PromotionsPage
     */
    handleCategoryClick = (category) => {
        this.setState({activeCategory: category});
    }

    /**
     * Gets the list of available categories for all promotions
     *
     * @memberof PromotionsPage
     */
    getCategories = (promotions) => {
        const categories = [];

        for (let i = 0; i < promotions.length; i++) {
            const promotion = promotions[i];
            for (let j = 0; j < promotion.categories.length; j++) {
                const category = promotion.categories[j];
                if (categories.indexOf(category) === -1) {
                    categories.push(category);
                }
            }
        }

        return categories;
    }

    /**
     * Gets the list of promotions filtered by the active category
     *
     * @memberof PromotionsPage
     */
    getPromotions = (promotions, category) => {
        if (category === null || category === "") {
            return promotions;
        } else {
            return promotions.filter((promotion) => {
                return promotion.categories.indexOf(category) !== -1;
            });
        }
    }

    render() {
        const promotionDetails = (this.state.selectedPromotion !== null)?(
            <PromotionDetails
                isOpen={this.state.showModal}
                toggle={this.handleToggle}
                promotion={this.state.selectedPromotion}
                title={content.promotionTitle}
            />
        ):(<div></div>);

        return (
            <div className="promotions-page">
                <PageHeader title={content.title} subTitles={content.subTitles} />
                <div className="promotions-page__promotions-title">
                    <h2 className="h2-text">
                        {content.promotionsTitle}
                    </h2>
                </div>
                <div className="promotions-page__promotions-container">
                    <PromotionsCategories
                        categories={this.getCategories(this.state.promotionsList)}
                        popularCategories={content.popularCategories}
                        activeCategory={this.state.activeCategory}
                        onCategoryClick={this.handleCategoryClick}
                    />
                    <PromotionsList
                        promotions={this.getPromotions(this.state.promotionsList, this.state.activeCategory)}
                        onPromotionClick={this.handlePromotionClick}
                    />
                </div>
                {promotionDetails}
            </div>
        );
    }
}
