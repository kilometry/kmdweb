import React from 'react';
import { Modal, ModalHeader, ModalBody } from 'reactstrap';

require('./promotionDetails.css');
require('../../typography.css');

export default class PromotionDetails extends React.Component {
    render() {
        let { isOpen, toggle, promotion, title } = this.props;

        return (
            <Modal
                isOpen={isOpen}
                className="modal-custom"
                toggle={toggle}
                size="xl"
                autoFocus={false}
            >
                <ModalHeader toggle={toggle} />
                <ModalBody>
                    <div>
                        <h1 className="h1-text">
                            {title}
                        </h1>
                        <div className="promotion-details__container">
                            <div className="promotion-details__image-container">
                                <img className="promotion-details__card-image" src={process.env.PUBLIC_URL + '/promotions/' + this.props.promotion.image} alt={this.props.promotion.name} />
                            </div>
                            <div className="promotion-details__content-container">
                                <div>
                                    <div className="h2-text">
                                        {promotion.fullTitle}
                                    </div>
                                    <div className="p-text">
                                        {promotion.fullDescription}
                                    </div>
                                </div>
                                <div className="promotion-details__promotion-text">
                                    {promotion.promotion}
                                </div>
                                <div className="promotion-details__button-container">
                                    <a className="btn btn-primary" href={promotion.url}>Перейти на сайт магазина</a>
                                </div>
                                <div className="promotion-details__button-container">
                                    <button className="btn btn-primary">Получить скидку</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </ModalBody>
            </Modal>
        );
    }
}
