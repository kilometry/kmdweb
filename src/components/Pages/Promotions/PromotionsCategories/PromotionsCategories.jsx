import React from 'react';
import classnames from 'classnames';

require('./promotionsCategories.css');

const PromotionsCategories = ({ categories, popularCategories, activeCategory, onCategoryClick }) => (
    <div className="promotion-categories-list">
        <div className="promotion-categories-list__cards-container">
            <div className="promotion-categories__section">
                <div className="promotion-categories__type-label">
                    Категории
                </div>
                {categories.map((category, index) => {
                    return (
                        <div
                            className={classnames("promotion-categories__category-label", {"promotion-categories__category-label--active": category === activeCategory})}
                            key={index}
                            onClick={() => {onCategoryClick(category)}}
                        >
                            {category}
                        </div>
                    );
                })}
            </div>

            <div>
                <div className="promotion-categories__type-label">
                    Популярные
                </div>
                {popularCategories.map((category, index) => {
                    return (
                        <div className="promotion-categories__category-label" key={index} onClick={() => {onCategoryClick(category)}}>
                            {category}
                        </div>
                    );
                })}
            </div>
        </div>
    </div>
);

export default PromotionsCategories;
