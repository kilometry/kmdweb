/* eslint-disable jsx-a11y/click-events-have-key-events, jsx-a11y/no-static-element-interactions */

import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import './promotionsList.css';
import '../../typography.css';

const PromotionsList = ({ promotions, onPromotionClick, showViewAll }) => (
    <div className="promotions-list">
        <div className="promotions-list__cards-container">
            {promotions.map(promotion => (
                <div className="promotions-list__card" key={promotion.key} onClick={() => onPromotionClick(promotion)}>
                    <div className="promotions-list__card-image-container">
                        <img
                            className="promotions-list__card-image"
                            src={`${process.env.PUBLIC_URL}/promotions/${promotion.image}`}
                            alt={promotion.title}
                        />
                    </div>
                    <div className="promotions-list__card-content-container">
                        <div className="promotions-list__card-name">{promotion.title}</div>
                        <div className="promotions-list__card-description">
                            <p>{promotion.description}</p>
                        </div>
                        <div className="promotions-list__card-button-container">
                            <button type="button" className="btn btn-primary">
                                Условия
                            </button>
                        </div>
                    </div>
                </div>
            ))}
            <div className="promotions-list__card" />
            <div className="promotions-list__card" />
            <div className="promotions-list__card" />
        </div>
        {showViewAll ? (
            <div className="promotions-list__card-show-all-container">
                <Link to="/promotions" className="a-text">
                    Смотреть все
                </Link>
            </div>
        ) : (
            <div />
        )}
    </div>
);

PromotionsList.propTypes = {
    promotions: PropTypes.array.isRequired,
    onPromotionClick: PropTypes.func.isRequired,
    showViewAll: PropTypes.bool
};

PromotionsList.defaultProps = {
    showViewAll: false
};

export default PromotionsList;
