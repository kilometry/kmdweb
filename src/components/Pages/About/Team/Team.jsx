import React from 'react';

require('./team.css');
require('../../typography.css');

const Team = ({ title, teamMembers }) => (
    <div className="team">
        <div className="team__header">
            <h2 className="h2-text">
                { title }
            </h2>
        </div>

        <div className="team__cards-container">
            { teamMembers.map((member, index) => {
                return (
                    <div className="team__card" key={index}>
                        <img className="team__card-image" src={ process.env.PUBLIC_URL + '/team/' + member.image } alt={member.name} />
                        <div className="team__card-name">
                            { member.name }
                        </div>
                        <div className="team__card-role">
                            { member.role }
                        </div>
                    </div>
                );
            }) }
        </div>
    </div>
);

export default Team;
