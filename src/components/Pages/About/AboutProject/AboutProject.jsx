import React from 'react';
import aboutProject from "./aboutProject.jpg";
import {Link} from 'react-router-dom';

require('./aboutProject.css');
require('../../typography.css');

const AboutProject = ({title, header, content, showMoreInfo}) => (
    <div className="about-project">
        <article className="about-project__article">
            <div className="about-project__header">
                <h3 className="h3-text">
                    { title }
                </h3>
                {
                    (showMoreInfo)?(<Link to="/about" className="about-project__more-info-button">Больше информации</Link>):(<div></div>)
                }
            </div>
            <div className="about-project__container">
                <div className="about-project__image-container">
                    <img src={aboutProject} alt="about project" className="about-project__image" />
                </div>
                <div>
                    <h2 className="h2-text">
                        { header }
                    </h2>

                    <div>
                        {content.map((paragraph, index) => {
                            return (
                                <p className="p-text" key={index}>
                                    { paragraph }
                                </p>
                            );
                        })}
                    </div>
                </div>
            </div>
        </article>
    </div>
);

export default AboutProject;
