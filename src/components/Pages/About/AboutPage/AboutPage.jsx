import React from 'react';
import PageHeader from '../../PageHeader/PageHeader';
import AboutProject from '../AboutProject/AboutProject';
import Team from '../Team/Team';
import Contacts from '../Contacts/Contacts';

import howWeWork from './howWeWork.jpg';

import content from '../../../../content/about-content.json';
import team from '../../../../content/team-list.json';

import './aboutPage.css';
import '../../typography.css';

const AboutPage = () => (
    <div className="about-page">
        <PageHeader title={content.title} subTitles={content.subTitles} />

        <article className="about-page__article">
            <h3 className="h3-text">{content.howWeWorkTitle}</h3>

            <div className="about-page__container">
                <div className="about-page__image-container">
                    <a href={content.howWeWorkVideoUrl} className="a-text">
                        <img src={howWeWork} alt="how we work" className="about-page__image" />

                        <div>{content.viewFullscreen}</div>
                    </a>
                </div>

                <div>
                    {content.howWeWork.map((paragraph, index) => {
                        return (
                            <p className="p-text" key={index}>
                                { paragraph }
                            </p>
                        );
                    })}
                </div>
            </div>
        </article>

        <AboutProject
            title={content.aboutTitle}
            header={content.aboutHeader}
            content={content.about}
            showMoreInfo={false}
        />

        <Team title={content.teamTitle} teamMembers={team.members} />

        <Contacts />
    </div>
);

export default AboutPage;
