import { Route, Redirect } from 'react-router-dom';
import React from 'react';
import PropTypes from 'prop-types';
import LoginStore from './../stores/Login/LoginStore';
import { connect } from '../utils/flux-utils';

const PrivateRoute = ({ redirect, currentUser, ...rest }) =>
    currentUser.authenticated ? <Route {...rest} /> : <Redirect to={redirect} />;

PrivateRoute.propTypes = {
    redirect: PropTypes.string.isRequired,
    currentUser: PropTypes.object.isRequired
};

export default connect(
    PrivateRoute,
    [LoginStore],
    (prev, props) => ({
        currentUser: LoginStore.getState().currentUser,
        ...props
    })
);
