import React from 'react';
import PropTypes from 'prop-types';

const Loading = ({ isVisible }) => isVisible && (
    <div className="loading">
        <div className="spinner" />
    </div>
);

Loading.propTypes = {
    isVisible: PropTypes.bool.isRequired
};

export default Loading;
