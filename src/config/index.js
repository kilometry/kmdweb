import logger from '../utils/logger';
import ajax from '../utils/ajax';

async function basePaths() {
    switch (process.env.NODE_ENV) {
        case 'development':
            logger.info('Using development configuration');
            return {
                apiBasePath: 'http://localhost:5001',
                identityBasePath: 'http://localhost:5000'
            };

        case 'production': {
            // assuming the front-end is always hosted within the API website
            const { origin, pathname } = window.location;
            const apiBasePath = `${origin}${pathname}`.replace(/\/+$/g, '');
            const { identityEndpoint } = await ajax.getJson(`${apiBasePath}/api/config`, false);
            return { apiBasePath, identityBasePath: identityEndpoint };
        }

        default:
            throw new Error(`Environment ${process.env.NODE_ENV} is not supported.`);
    }
}

const config = basePaths().then(({ apiBasePath, identityBasePath }) => ({
    openIdConfigurationEndpoint: `${identityBasePath}/.well-known/openid-configuration`,
    apiEndpoint: `${apiBasePath}/api`,
    identityApiEndpoint: `${identityBasePath}/api`,
    clientId: 'js-pass',
    clientSecret: 'js-secret'
}));

export default config;
