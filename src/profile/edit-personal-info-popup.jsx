import React from 'react';
import PropTypes from 'prop-types';
import { Modal, ModalHeader, ModalBody } from 'reactstrap';
import MaskedInput from 'react-maskedinput';
import { handleChange } from '../utils/react-utils';
import ajax from '../utils/ajax';
import asyncConfig from '../config';
import { currentUser } from '../user-menu';
import logger from '../utils/logger';
import { extractErrorsFromResponse, getErrorMessages } from '../common/errors';
import { FormControl, GeneralError } from '../components/form';
import Loading from '../components/loading';

export default class EditPersonalInfoPopup extends React.Component {
    static propTypes = {
        isOpen: PropTypes.bool.isRequired,
        toggle: PropTypes.func.isRequired,
        user: PropTypes.object.isRequired
    }

    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            errors: {},
            ...this._stateFromProps(props)
        };

        this._handleChange = name => evt => handleChange(this, evt, name);
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            ...this._stateFromProps(nextProps),
            errors: {}
        });
    }

    _stateFromProps(props) {
        return {
            firstName: props.user.firstName,
            lastName: props.user.lastName,
            patronym: props.user.patronym,
            email: props.user.email,
            phoneNumber: props.user.phoneNumber
        };
    }

    async _onSaveButtonClick() {
        try {
            const user = {
                firstName: this.state.firstName,
                lastName: this.state.lastName,
                patronym: this.state.patronym,
                email: this.state.email,
                // TODO extract to a common module and use it in registration-form as well.
                phoneNumber: this.state.phoneNumber.replace(/[()_\-\s]/g, ''),
            };

            this.setState({ isLoading: true });
            const config = await asyncConfig;
            const result = await ajax.postForm(`${config.apiEndpoint}/Account`, user);
            const updatedUser = {
                ...currentUser.get(),
                ...result
            };

            currentUser.set(updatedUser);
            this.props.toggle();
        } catch (e) {
            logger.error(e);
            this.setState({ errors: extractErrorsFromResponse(e) });
        } finally {
            this.setState({ isLoading: false });
        }
    }

    render() {
        return (
            <Modal
                isOpen={this.props.isOpen}
                toggle={() => this.props.toggle()}
                className="modal-custom"
                size="lg"
                autoFocus={false}
            >
                <ModalHeader toggle={() => this.props.toggle()} />
                <ModalBody>
                    <Loading isVisible={this.state.isLoading} />

                    <div className="modal-wrap text-center">
                        <div className="modal-wrap-title">Личные данные</div>
                    </div>

                    <form>
                        <GeneralError errors={this.state.errors} />

                        <FormControl
                            label="Ваша фамилия*:"
                            type="text"
                            className="form-control"
                            value={this.state.lastName}
                            placeholder="Иванов"
                            onChange={this._handleChange('lastName')}
                            errors={getErrorMessages(this.state.errors, 'lastName')}
                        />

                        <FormControl
                            label="Ваше имя*:"
                            type="text"
                            className="form-control"
                            value={this.state.firstName}
                            placeholder="Иван"
                            onChange={this._handleChange('firstName')}
                            errors={getErrorMessages(this.state.errors, 'firstName')}
                        />

                        <FormControl
                            label="Ваше отчество*:"
                            type="text"
                            className="form-control"
                            value={this.state.patronym}
                            placeholder="Иванов"
                            onChange={this._handleChange('patronym')}
                            errors={getErrorMessages(this.state.errors, 'patronym')}
                        />

                        <FormControl
                            label="Email, по которому с вами можно связаться*:"
                            type="text"
                            className="form-control"
                            value={this.state.email}
                            placeholder="email@host.com"
                            onChange={this._handleChange('email')}
                            errors={getErrorMessages(this.state.errors, 'email')}
                        />

                        <FormControl
                            label="Номер телефона*:"
                            mask="+375 (11) 111-11-11"
                            value={this.state.phoneNumber}
                            onChange={this._handleChange('phoneNumber')}
                            renderControl={props => <MaskedInput {...props} />}
                            errors={getErrorMessages(this.state.errors, 'phoneNumber')}
                        />

                        <div className="form-group text-center">
                            <button type="submit" className="btn btn-primary" onClick={() => this._onSaveButtonClick()}>
                                Сохранить данные
                            </button>
                        </div>

                    </form>
                </ModalBody>
            </Modal>
        );
    }
}
