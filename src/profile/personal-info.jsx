/* eslint-disable
    no-return-assign,
    jsx-a11y/no-static-element-interactions,
    jsx-a11y/click-events-have-key-events */

import React from 'react';
import classnames from 'classnames';
import uploadPhotoIcon from '../img/upload-photo-icon.png';
import noPhoto from '../img/no-photo.png';
import { currentUser } from '../user-menu';
import EditPersonalInfoPopup from './edit-personal-info-popup';
import asyncConfig from './../config';
import ajax from './../utils/ajax';
import logger from './../utils/logger';
import { GeneralError } from './../components/form';
import { extractErrorsFromResponse } from '../common/errors';
import ChangePassword from '../components/Pages/Profile/ChangePassword/ChangePassword';

export default class PersonalInfo extends React.Component {
    state = {
        user: {},
        errors: {},
        editInfoPopupIsOpen: false,
        isChangePasswordPopupOpen: false,
        balance: {}
    };

    async componentWillMount() {
        currentUser.subscribe(user => {
            if (user.authenticated) {
                this.setState({ user });
            }
        });

        const config = await asyncConfig;
        const balance = await ajax.getJson(`${config.apiEndpoint}/account/balance`);
        this.setState({ balance });
    }

    _toggleEditInfoPopup() {
        this.setState({ editInfoPopupIsOpen: !this.state.editInfoPopupIsOpen });
    }

    _onUploadPhotoClick() {
        this._filePicker.click();
    }

    async _onPhotoChange() {
        if (this._filePicker.files.length > 0) {
            try {
                const config = await asyncConfig;
                const result = await ajax.postForm(`${config.apiEndpoint}/avatar`, {
                    file: this._filePicker.files[0]
                });

                const updatedUser = {
                    ...currentUser.get(),
                    ...result
                };

                currentUser.set(updatedUser);
                this.setState({ errors: {} });
            } catch (e) {
                this.setState({
                    errors: extractErrorsFromResponse(e, 'Во время загрузки фото произошла ошибка.')
                });
                logger.error(e);
            } finally {
                this._filePicker.value = '';
            }
        }
    }

    toggleChangePasswordPopup = () => {
        this.setState({ isChangePasswordPopupOpen: !this.state.isChangePasswordPopupOpen });
    };

    render() {
        const profilePhotoClasses = classnames([
            'd-flex',
            'flex-column',
            'position-relative',
            'text-center',
            'rounded',
            'profile-info-section',
            'profile-photo',
            'have-photo'
        ]);

        return (
            <div className="col-12 col-md-3 personal-info">
                <div className={profilePhotoClasses}>
                    <div className="photo">
                        <img src={this.state.user.avatarUrl || noPhoto} alt="Фото" className="img-fluid rounded" />
                    </div>

                    <div
                        className="d-flex flex-column justify-content-center px-5 py-4 upload-photo"
                        onClick={() => this._onUploadPhotoClick()}
                    >
                        <div className="no-photo">
                            <img src={uploadPhotoIcon} alt="Нету фото" />
                        </div>
                        <div className="mt-1 no-photo-text">Нажмите здесь чтобы загрузить фото</div>
                    </div>

                    <input
                        type="file"
                        className="d-none"
                        accept="image/*"
                        ref={el => (this._filePicker = el)}
                        onChange={() => this._onPhotoChange()}
                    />

                    <GeneralError errors={this.state.errors} />
                </div>
                <div className="text-center profile-info-section">
                    <div className="rounded current-balance">
                        Ваш баланс: <span>{`${this.state.balance.remainingKilometers || 0} км.`}</span>
                    </div>
                    <div className="mt-3 total-paid">
                        Всего оплачено: <span>{`${this.state.balance.totalPaidKilometers || 0} км.`}</span>
                    </div>
                </div>
                <div className="profile-info-section profile-info">
                    <div className="info-field">
                        <div>Имя:</div>
                        <div>{this.state.user.firstName}</div>
                    </div>
                    <div className="info-field">
                        <div>Фамилия:</div>
                        <div>{this.state.user.lastName}</div>
                    </div>
                    <div className="info-field">
                        <div>Отчество:</div>
                        <div>{this.state.user.patronym}</div>
                    </div>
                </div>
                <div className="profile-info-section profile-info">
                    <div className="info-field">
                        <div>Email:</div>
                        <div>{this.state.user.email}</div>
                    </div>
                    <div className="info-field">
                        <div>Телефон:</div>
                        <div>{this.state.user.phoneNumber}</div>
                    </div>
                </div>
                <div className="text-center profile-actions">
                    <button type="button" className="btn btn-primary" onClick={() => this._toggleEditInfoPopup()}>
                        Изменить данные
                    </button>
                    <button type="button" className="btn btn-link" onClick={this.toggleChangePasswordPopup}>
                        Изменить пароль
                    </button>
                </div>
                <EditPersonalInfoPopup
                    isOpen={this.state.editInfoPopupIsOpen}
                    toggle={() => this._toggleEditInfoPopup()}
                    user={this.state.user}
                />
                <ChangePassword isOpen={this.state.isChangePasswordPopupOpen} toggle={this.toggleChangePasswordPopup} />
            </div>
        );
    }
}
