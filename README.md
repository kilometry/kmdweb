This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

You also need [yarn](https://yarnpkg.com/en/) to be installed globally:
```
npm install yarn@latest -g
```

Then in the root folder of the repository you need to execute
```
yarn install
```

Then you can use the following commands:

 * `yarn build` - builds a package for deployment (will be placed in `/build` folder)
 * `yarn start` - starts a dev server (with source files watching and hot reloading)