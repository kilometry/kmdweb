/* eslint-disable import/no-extraneous-dependencies, no-console */

const fs = require('fs');
const archiver = require('archiver');

const filename = process.argv[2];

const output = fs.createWriteStream(`${__dirname}/../${filename}`);
const archive = archiver('zip', {
    zlib: { level: 9 }
});

output.on('close', () => {
    console.log(`${filename} of ${archive.pointer()} bytes was created.`);
});

archive.on('warning', err => {
    if (err.code === 'ENOENT') {
        console.warn(JSON.stringify(err, null, 4));
    } else {
        throw err;
    }
});

archive.on('error', err => {
    throw err;
});

console.log('Zipping the build results...');

archive.pipe(output);
archive.directory(`${__dirname}/../build/`, false);

archive.finalize();
